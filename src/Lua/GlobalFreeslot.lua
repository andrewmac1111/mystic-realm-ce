-- placed here so it doesn't use it's own file lol
sfxinfo[sfx_kc5c].caption = "Shrine Activated"

--Credit to Motd for this
--Credit to Lach for the OG

--Thank you so much Motd for letting me use this.
--This saved me a massive headache of trying to
--make my own custom font function. - Ryuko

-- v: the drawer
-- x, y: screen coordinates
-- text: the string to display
-- font: the patch prefix
-- flags: just like any other HUD function
-- alignment: -1 = right-aligned, 1 = left-aligned
-- color: a skincolor

local function DrawMotdString(v, x, y, scale, text, font, flags, alignment, color)
	local right
	local colormap = v.getColormap(0, color)
	local start
	local finish
	alignment = $ or 1
	color = $ or 0
	right = alignment < 0
	text = tostring(text)

	if right then
		start = text:len()
    	finish = 1
	else
		start = 1
		finish = text:len()
	end

	for i = start, finish, alignment do
		local letter = font .. text:sub(i, i)
		if not v.patchExists(letter) then continue end

		local patch = v.cachePatch(letter)

		if right then -- right aligned, change offset before drawing
			x = $ - patch.width*scale
		end

		v.drawScaled(x, y, scale, patch, flags, colormap)

		if not right then -- left aligned, change offset after drawing
			x = $ + patch.width*scale
		end
	end
end

-- Borrowed from MapVote.lua
--IntToExtMapNum(n)
--Returns the extended map number as a string
--Returns nil if n is an invalid map number
local function IntToExtMapNum(n)
	if n < 0 or n > 1035 then
		return nil
	end
	if n < 10 then
		return "MAP0" + n
	end
	if n < 100 then
		return "MAP" + n
	end
	local x = n-100
	local p = x/36
	local q = x - (36*p)
	local a = string.char(65 + p)
	local b
	if q < 10 then
		b = q
	else
		b = string.char(55 + q)
	end
	return "MAP" + a + b
end

--From CobaltBW. Thank you my dude!
--This is used to turn a level header variable into a table
--via detecting a "," as a separator.
local function GetNumberList(str)
	local t = {}
	while str do
		local sep = string.find(str,'%,')
		if sep != nil then
			local arg = string.sub(str,0,sep-1)
			local tag = tonumber(arg)
			if tag then
				table.insert(t, tag)
			elseif tag != 0 then
				print('Invalid argument '..arg)
				break
			end
			str = string.sub($,sep+1)
		else
			local tag = tonumber(str)
			if tag then
				table.insert(t, tag)
			end
			break
		end
	end
	return t
end

rawset(_G, "DrawCustomString", DrawMotdString)
rawset(_G, "GetNumberList", GetNumberList)
rawset(_G, "IntToExtMapNum", IntToExtMapNum)

-- Misc variables used throughout multiple scripts
rawset(_G, "pi", 22*FRACUNIT/7) -- Used in multiple objects and bosses
rawset(_G, "dispstaticlogo", false) -- Credits
rawset(_G, "debugmenu", false) -- Used to hide titlescreen elements for debug menus

-- Menu Table (Contains every single menu that the game can pull up)
-- Each of these entries get a value set on menu init
-- That value being the actual menu table for each menu
rawset(_G, "menutable", {
	-- Main Menu
	"mainmenu",
	"multiplayer", -- Links to OG SRB2 menu
	"extras",
	"addons", -- Links to OG SRB2 menu
	"quit",

	-- Singleplayer Menu
	"singleplayer",
		------Save select------
			"saveselect",
			"characterselect", -- Save creation
			"episodeselect",
		------Save select------
	"recordattack",
	"marathonmode",
	"statistics",

	-- Options
	"options",
	"controloptions", -- Links to OG SRB2 menu
	"videooptions",
	"soundoptions", -- Links to OG SRB2 menu
	"dataoptions",
	
	-- Advanced Options
	"advancedoptions",
	"fulloptions", -- Links to OG Options menu
	"fullsrb2menu" -- Links to OG SRB2 menu
})
