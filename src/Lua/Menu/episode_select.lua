/*
Episode Selection Menu

Used for selection between second quest and regular mode
after game complete

Original menu framework taken from Ultimate Randomizer
A lot has been modified from that original script

Created by Ash
OG by Nightwolf
*/


addHook("MapLoad", function(p)
	for player in players.iterate
		if gamemap == 99 then
			player.urhudon = true
		end
		if gamemap != 99 then
			player.urhudon = false
		end
	end
end)

local HUDX = 0
local HUDY = 0
local HUDWINWIDTH = 320 //Maximum 320 (actual size is 4 units smaller due to the outline)
local HUDWINHEIGHT = 200 //Maximum 200
local BGHEIGHT = 200
local HUDTEXTHEIGHT = 30 //Height between HUD texts
local MAXELEMENTS = (HUDWINHEIGHT/HUDTEXTHEIGHT)-2 //Currently 8
local selx = 0
local sely = 0
local textwidth = 0

local HBUTTON_UP = 1
local HBUTTON_DOWN = 2
local HBUTTON_JUMP = 3
local HBUTTON_SPIN = 4

-- ease animation vars
local text_animdur = 1*TICRATE
local text_maxslide = 160
local text_animtime = 0

local animtime_capped = nil
local animate_percentage = nil

local function listnext(list)
	if type(list) != "table" return end
	if not list.selection then list.selection = 1 end
	if list.selection < #list.contents then
		list.selection = $1 + 1
	else
		list.selection = 1
	end
	list.selsound()
end

local function listprev(list)
	if type(list) != "table" return end
	if not list.selection then list.selection = 1 end
	if list.selection < 2 then
		list.selection = #list.contents
	else
		list.selection = $1 - 1
	end
	list.selsound()
end

local function clistmove(num,limit,fwd)
	if not num or not limit then return 1 end
	if fwd then
		if num < limit then return num+1 else return 1 end
	else
		if num < 2 then return limit else return num-1 end
	end
end

local function ishbuttonreleased(p,button)
	if button == HBUTTON_UP then
		if p.cmd.forwardmove <= 0 then return true end
	elseif button == HBUTTON_DOWN then
		if p.cmd.forwardmove >= 0 then return true end
	elseif button == HBUTTON_JUMP then
		if p.cmd.buttons & BT_JUMP == 0 then return true end
	elseif button == HBUTTON_SPIN then
		if p.cmd.buttons & BT_USE == 0 then return true end
	end
	return false
end

local function reversenum(tab,index)
	if not tab then return end
	if not index then return end
	if tab[index] == nil then return end
	if tab[index] == 0 or tab[index] == false
		tab[index] = 1
		return
	else
		tab[index] = 0
		return
	end
end

local function changelnum(huddata,num)
	if not huddata then return end
	if not num then num = 1 end
	huddata.listnum = num
	huddata.selection = 1
end

local function hudtimegoleft(huddata,self,time,minsec,maxmin)
	local tsec = time % 60
	local tmin = time / 60
	if self.selpart < 1 then return time end
	if self.issel then
		if self.selpart == 1 then 
			tmin = clistmove(tmin+1,maxmin,false)-1
			if tmin == 0 and tsec < minsec then tsec = 1 end
		elseif self.selpart == 2 then 
			if tmin == 0 then tsec = clistmove(tsec-minsec+1,60,false)+minsec-1 % 60
			else tsec = clistmove(tsec+1,60,false)-1 end
		end
	else 
		self.selpart = clistmove(self.selpart,2,false) 
	end
	return tmin * 60 + tsec
end

local function hudtimegoright(huddata,self,time,minsec,maxmin)
	local tsec = time % 60
	local tmin = time / 60
	if self.selpart < 1 then return time end
	if self.issel then
		if self.selpart == 1 then 
			tmin = clistmove(tmin+1,maxmin,true)-1
			if tmin == 0 and tsec < minsec then tsec = 1 end
		elseif self.selpart == 2 then
			if tmin == 0 then tsec = clistmove(tsec-minsec+1,60,true)+minsec-1 % 60
			else tsec = clistmove(tsec+1,60,true)-1 end
		end
	else 
		self.selpart = clistmove(self.selpart,2,true) 
	end
	return tmin * 60 + tsec
end

local hudcontents = {}
hudcontents[1] = {center = 2}
hudcontents[1][1] = {
	startstring = "Episode Select",
	string = "Lock-On",
	go = function(huddata, p)
		print("Starting Lock On!")
		G_SetCustomExitVars(01,1)
		huddata.selection = -1
		G_ExitLevel()
	end,
	back = function()
		return
	end,
}
hudcontents[1][2] = {
	string = "Main Quest",
	go = function(huddata, p)
		print("Starting Main Quest!")
		G_SetCustomExitVars(101,1)
		huddata.selection = -1
		G_ExitLevel()
	end,
	back = function()
		return
	end,
}
hudcontents[1][3] = {
	string = "Second Quest",
	go = function(huddata)
		print("Starting Second Quest!")
		G_SetCustomExitVars(101,1)
		huddata.selection = -1
		G_ExitLevel()
	end,
	back = function()
		return
	end,
}

//Credit to Flame for the 3 lines of code below
addHook("JumpSpecial", function(p) return p.urhudon end)
addHook("SpinSpecial", function(p) return p.urhudon end)
addHook("JumpSpinSpecial", function(p) return p.urhudon end)

addHook("PlayerThink",function(p)
	if not p.urhudon then return end
	if not p.urhud then 
		p.urhud = {} 
		p.urhud.selection = 1
		p.urhud.listnum = 1
		p.urhud.pressed = 0
		p.urhud.sidepress = false //Left and right presses are checked differently as L/R functions require constant input
		p.urhud.selsound = function()
			S_StartSound(nil,sfx_menu1,p)
		end
	end
	p.urhud.contents = hudcontents[p.urhud.listnum]
	local sel = p.urhud.selection
	local cont = p.urhud.contents
	local sidemove = p.cmd.sidemove
	local fwdmove = p.cmd.forwardmove
	if p.mo then
		p.mo.momx = 0
		p.mo.momy = 0
	end
	if p.cmd.buttons & BT_CUSTOM1 != 0 or sel < 0 then
		p.urhudon = false
		p.urhud = nil
		return
	elseif sidemove > 0 then
		if p.urhud.sidepress then p.urhud.sidepress = false; return end
		if cont[sel].right then cont[sel].right(p.urhud,cont[sel]) end
		p.urhud.sidepress = true
	elseif sidemove < 0 then
		if p.urhud.sidepress then p.urhud.sidepress = false; return end
		if cont[sel].left then cont[sel].left(p.urhud,cont[sel]) end
		p.urhud.sidepress = true
	elseif p.urhud.pressed then
		if ishbuttonreleased(p,p.urhud.pressed) then p.urhud.pressed = 0 end
		return
	elseif fwdmove > 0 then
		p.urhud.pressed = HBUTTON_UP
		if cont[sel].up then cont[sel].up(p.urhud,cont[sel]) else listprev(p.urhud) selx = HUDX+4 end
	elseif fwdmove < 0 then
		p.urhud.pressed = HBUTTON_DOWN
		if cont[sel].down then cont[sel].down(p.urhud,cont[sel]) else listnext(p.urhud) selx = HUDX+4 end
	elseif p.cmd.buttons & BT_JUMP != 0 then
		p.urhud.pressed = HBUTTON_JUMP
		if cont[sel].go then cont[sel].go(p.urhud,cont[sel]) end
	elseif p.cmd.buttons & BT_USE != 0 then
		p.urhud.pressed = HBUTTON_SPIN
		if cont[sel].back then cont[sel].back(p.urhud,cont[sel]) end
	end
end)

hud.add(function(v,p,cam)
	if not p.urhudon or not p.urhud then return end
	if not p.urhud.selection or not p.urhud.contents then return end
	if p.urhud.selection < 1 then return end
	v.drawFill(HUDX,HUDY,HUDWINWIDTH,HUDWINHEIGHT,159)
	v.draw(30,BGHEIGHT+10,v.cachePatch("MYSTIC1"))
	v.draw(290,BGHEIGHT+10,v.cachePatch("MYSTIC1"),V_FLIP)
	v.draw(30,BGHEIGHT-310,v.cachePatch("MYSTIC1"))
	v.draw(290,BGHEIGHT-310,v.cachePatch("MYSTIC1"),V_FLIP)
	v.drawFill(-170,-100,200,400,31)
	v.drawFill(290,-100,200,400,31)
	local textx = 160 //X of value description (left border)
	if selx == 0 then
		selx = HUDX+4 //X of the value selected
	end
	local startstr = hudcontents[p.urhud.listnum][1].startstring
	local arrangenum = hudcontents[p.urhud.listnum].center
	local valuex = HUDX+HUDWINWIDTH-4 //X of value string (right border)
	local starttexty = HUDY+4 //Start Y of HUD text
	if p.urhud.contents[p.urhud.selection].startstring then startstr = p.urhud.contents[p.urhud.selection].startstring end
	v.drawString(HUDX+(HUDWINWIDTH/2),starttexty,"\x88"..tostring(startstr),0,"center")
	local elemy = starttexty+HUDTEXTHEIGHT //Start Y of first interactive element (changes in the next for loop)
	if sely == 0 then
		sely = elemy
	end
	local elemlimit = min(#p.urhud.contents,MAXELEMENTS)
	for f = 1,elemlimit do
		if p.urhud.selection < f then
			if elemy == 90+HUDTEXTHEIGHT then
				elemy = $1+HUDTEXTHEIGHT
			else
				elemy = 90+HUDTEXTHEIGHT
			end
		elseif p.urhud.selection > f then
			if p.urhud.selection-f == 1 then
				elemy = $1+HUDTEXTHEIGHT
			else
				elemy = $1+((p.urhud.selection-f)/HUDTEXTHEIGHT)
			end
		end

		if p.urhud.selection == f then 
			v.drawNameTag(textx-(v.nameTagWidth(p.urhud.contents[f].string)/2),90,p.urhud.contents[f].string,0,SKINCOLOR_WHITE,SKINCOLOR_EMERALD)
			textwidth = v.nameTagWidth(p.urhud.contents[f].string)
		else
			v.drawScaledNameTag(((textx-v.nameTagWidth(p.urhud.contents[f].string)/FRACUNIT/2)*FRACUNIT/2)+40*FRACUNIT,elemy*(FRACUNIT),p.urhud.contents[f].string,0,FRACUNIT/2,SKINCOLOR_WHITE, SKINCOLOR_BLACK) 
		end
		v.draw(textx-(textwidth/2)-23, 90, v.cachePatch("CHAOS3"))
		v.draw(textx+(textwidth/2)+10, 90, v.cachePatch("CHAOS3"))
		if p.urhud.contents[f].printstr then 
			v.drawString(valuex,elemy,p.urhud.contents[f].printstr(p.urhud,p.urhud.contents[f]),0,"right") 
		end
	end
end,"game")

addHook("ThinkFrame", do
	for player in players.iterate do
		if not player.urhudon or not player.urhud then return end
		if BGHEIGHT == 220 then
			BGHEIGHT = -100
		else
			BGHEIGHT = $ + 2
		end
	end
end)