--[[SIMPLE MENU v1.7.2
follow the comments to use this Lua in your mod, if you find any issue let me know!
	-Felix44]]--

local menu

local up1, up2 = 0
local down1, down2 = 0
local left1, left2 = 0
local right1, right2 = 0
local jump1, jump2 = 0
local spin1, spin2 = 0
local con1, con2 = 0

local function notImplemented()
	print("Not Implemented")
end

local function stubFunc() end

local function QuitGame()
	COM_BufInsertText(consoleplayer, "quit")
end

--MENU FLAGS
rawset(_G, "MNF_SLIDER", 1) --makes a setting display a slider instead of a string as its value (works the same way as the "slider" parameter)
rawset(_G, "MNF_NOSAVE", 2) --a value with this flag will not be saved/loaded from the save file
rawset(_G, "MNF_SHOWVALUE", 4) --forces the setting to display it's value, since only function settings and page changer settings don't display theirs it should only be used for those (NOTE: function settings don't have any option parameter set to them by default, don't forget to include it!)
rawset(_G, "MNF_CHANGEPAGE", 8) --settings with this flag will change the page pointer, allowing to make more complex menus with more pages and stuff, selcting these settings will change the menu page to the one defined in its "value" parameter, I suggest to give these settings the MNF_NOSAVE flag too since usually the "value" of this setting wouldn't be able to change and thus wouldn't need to be saved

--[[player.menu.contents is the main part of the menu, every key in this table will refer to a page, if you only want a single page make a single key [1],
	you can put the elements of the menu pages inside these keys, you can have as many elements as you want, but it might be best not to have more than 10 for each page
	"value" sets their value, starting from 0, set this value to the setting's default value
		use this for your checks in your Lua, an example is at the end of this Lua
	"name" is the name of the setting that will appear on the menu
	"description" is the small text that will appear on the bottom of the screen
	"toggledby" is an extra parameter for subsections, set it to the number of the setting which your subsection setting should be depending on
		do note that if the setting that controls the subsecton is off, it will make the subsection not modifiable, but it will NOT set it to false automatically, remember that when making the checks in your other Luas
	"func" is an extra parameter for function setting, these settings can't be toggled on/off, selecting them will call the given function instead
	"maxvalue" is for a setting with more than 2 options, set this parameter to the amount of options your setting can be
		do note that "value" starts from 0, so if for example you want a 3 option setting "value" will start from 0 and reach it's max at 2 (maxvalue would still have to be set to 3 though)
		if this parameter is omitted it will be set to 2 (equivalent to an ON/OFF setting)
	"options" must be a table containing the names of all the possible setting's options, this table should be as long as maxvalue (example: if maxvalue is 3 the table should contain 3 strings)
		if this parameter is omitted it will be set to {"OFF", "ON"}
	"slider" is a boolean parameter that when set to true it displays a slider instead of a string, thus "options" can be omitted
		this parameter is deprecated since MNF_SLIDER now exists, but it can still be used and it will work the same way
	"flags" is the parameter for menu flags (MNF_*), you can see them and what they do near the start of the file
	"vflags" is the parameter for video flags (V_*), you could use this for example to give the text a different color when not selected,
		this parameter accepts any video flags, but it's primarely meant for color flags (V_*MAP) and it might break with other video flags
	"selectedvflags" is a parameter that gives the specified video flags (V_*) when the setting is selected, this default to V_YELLOWMAP if omitted
		this parameter accepts any video flags, but it's primarely meant for color flags (V_*MAP) and it might break with other video flags
	"pagename" is a parameter that sets the name of the page, this defaults to "Page " + the "value" parameter if omitted
		this paramter will only work with MNF_CHANGEPAGE settings
]]


addHook("PreThinkFrame", function()
	menu.timer = max($-1, 0)
end)


addHook("PlayerThink", function(player)
	if menu.show then
		player.powers[pw_nocontrol] = 1
		player.powers[pw_flashing] = 1
		player.mo.momx = 0
		player.mo.momy = 0
		player.mo.momz = 0
	end
end)

addHook("MapLoad", do
	-- Movement keys
	up1, up2 = input.gameControlToKeyNum(GC_FORWARD)
	down1, down2 = input.gameControlToKeyNum(GC_BACKWARD)
	left1, left2 = input.gameControlToKeyNum(GC_STRAFELEFT)
	right1, right2 = input.gameControlToKeyNum(GC_STRAFERIGHT)

	-- Option keys
	jump1, jump2 = input.gameControlToKeyNum(GC_JUMP)
	spin1, spin2 = input.gameControlToKeyNum(GC_SPIN)

	-- We still want the console to be usable
	con1, con2 = input.gameControlToKeyNum(GC_CONSOLE)
end)

addHook("MapLoad", do
	if gamemap == 784 then
		menu = {}
		menu.pointer = 1
		menu.pagepointer = 1
		menu.pagename = "Page 1"
		menu.show = false
		menu.timer = 0
		menu.contents = {
			[1] = {
				title = "Main Menu";
				{value = 2, name = "Singleplayer", description = "Play the main story mode alone", flags = MNF_CHANGEPAGE|MNF_NOSAVE, pagename = "Page 2"},
				{value = 1, name = "Multiplayer", description = "Battle your friends or play the story together", func = notImplemented},
				{value = 1, name = "Extras", description = "Check out the unlockable extras", func = notImplemented},
				{value = 1, name = "Options", description = "Change the game options", func = notImplemented},
				{value = 1, name = "Addons", description = "Load custom addons/characters", func = notImplemented},
				{value = 3, name = "Quit", description = "Return to desktop", flags = MNF_CHANGEPAGE|MNF_NOSAVE, pagename = "Page 3"}
			},
			[2] = {
				title = "Singleplayer";
				{value = 1, name = "Save Select", description = "Continue your last playthrough or start a new one", func = notImplemented},
				{value = 1, name = "Record Attack", description = "Test your knowledge by racing through the levels", func = notImplemented},
				{value = 1, name = "Marathon Mode", description = "Speedrun through the entire game with no breaks", func = notImplemented},
				{value = 1, name = "Statistics", description = "Check out your emblem count or total playtime", func = notImplemented},
				{value = 1, name = "Back", description = "Return to the Main Menu", flags = MNF_CHANGEPAGE|MNF_NOSAVE, pagename = "Page 1"}
			},
			[3] = {
				title = "Are you sure you want to quit?",
				menustyle = "msgbox";
				{value = 1, name = "Yes", description = "q", func = QuitGame},
				{value = 1, name = "No", description = "q", flags = MNF_CHANGEPAGE|MNF_NOSAVE, pagename = "Page 1"}
			}
		}
		io.openlocal("client/MRCE/Name.dat", "a+"):close() --you can change "Name" to something else, change it for the line below too if you do
		local file = io.openlocal("client/MRCE/Name.dat", "r+")
		for e = 1,#menu.contents do
			for i = 1,#menu.contents[e] do
				if not menu.contents[e][i].flags then menu.contents[e][i].flags = 0 end
				if not menu.contents[e][i].selectedvflags then menu.contents[e][i].selectedvflags = V_YELLOWMAP end
				if not menu.contents[e][i].pagename then menu.contents[e][i].pagename = "Page "..menu.contents[e][i].value end
				if menu.contents[e][i].func then continue end
				if not menu.contents[e][i].maxvalue then
						menu.contents[e][i].maxvalue = 2
				elseif menu.contents[e][i].maxvalue == 1 then
					CONS_Printf(consoleplayer, "\133ERROR IN PAGE "..e.."\nSetting "..i.."'s maxvalue is 1! Are you sure it's intentional?")
					menu.contents[e][i].maxvalue = 2
					CONS_Printf(consoleplayer, "\133Maxvalue was set to 2.")
				elseif menu.contents[e][i].maxvalue < 1 then
					CONS_Printf(consoleplayer, "\133ERROR IN PAGE "..e.."\nSetting "..i.."'s maxvalue("..menu.contents[e][i].maxvalue..") is too small! Are you sure it's intentional?")
					menu.contents[e][i].maxvalue = 2
					CONS_Printf(consoleplayer, "\133Maxvalue was set to 2.")
				end
				if not menu.contents[e][i].options then
					menu.contents[e][i].options = {"OFF", "ON"}
				elseif #menu.contents[e][i].options < menu.contents[e][i].maxvalue and not (menu.contents[e][i].slider or menu.contents[e][i].flags & MNF_SLIDER) then
					CONS_Printf(consoleplayer, "\133ERROR IN PAGE "..e.."\nSetting "..i.."'s options can't be fewer than maxvalue("..menu.contents[e][i].maxvalue..")!")
					for a =1,menu.contents[e][i].maxvalue do
						if menu.contents[e][i].options[a] then continue end
						menu.contents[e][i].options[a] = "Missing!"
					end
				end
				if menu.contents[e][i].flags & MNF_NOSAVE then continue end
				local text = file:read("*l")
				if text then
					menu.contents[e][i].value = tonumber(text)	
				end
			end
		end
		menu.show = not $
	end
end)

addHook("KeyDown", function(key)
	if titlemapinaction then
		local contents = menu.contents[menu.pagepointer]
		if menu.timer then 
			return true
		end
		if key.num == up1
		or key.num == up2 then
			menu.pointer = max($-1, 1)
			menu.timer = 5
			S_StartSound(nil, sfx_menu1)
			return true
		elseif key.num == down1
		or key.num == down2 then
			menu.pointer = min($+1, #contents)
			menu.timer = 5
			S_StartSound(nil, sfx_menu1)
			return true
		end
		if not key.num == con1
		or not key.num == con2 then
			return true
		end
	end
end)

COM_AddCommand("showmenu", function(player) --you can change the command's name to anything you want
	menu.show = not $
	if menu.show == false then
		local file = io.openlocal("client/MRCE/Name.dat", "w") --if you changed "Name" in the other lines, change it here too!
		for e = 1,#menu.contents do
			for i = 1,#menu.contents[e] do
				if menu.contents[e][i].func or menu.contents[e][i].flags & MNF_NOSAVE then continue end
				file:write(menu.contents[e][i].value.."\n")
			end
		end
		file:close()
	end
end, COM_LOCAL)

hud.add(function(v,player) --this draws the menu on the screen, you can change the values to what you prefer if you know what you are doing
	if not menu.show then return end
	local color = 115
	v.drawFill(60, 25, 200, 25+(10*#menu.contents[menu.pagepointer]), color)
	v.drawString(160, 30, menu.contents[menu.pagepointer].title, 0, "center")
	local pagename = menu.pagename
	local menutype = nil
	if #menu.contents == 1 then pagename = "" end
	v.drawString(257, 45+(10*#menu.contents[menu.pagepointer]), pagename, 0, "small-right")
	for i = 1,#menu.contents[menu.pagepointer] do
		local thecolor = menu.contents[menu.pagepointer][i].vflags or 0
		local thex = 0
		if menu.pointer == i then thecolor = menu.contents[menu.pagepointer][i].selectedvflags end
		if menu.contents[menu.pagepointer][i].toggledby then
			thex = 10
			if menu.contents[menu.pagepointer][menu.contents[menu.pagepointer][i].toggledby].value == 0 then
				thecolor = $|V_TRANSLUCENT
			end
		end
		local thename = "Missing name!"
		if menu.contents[menu.pagepointer][i].name and menu.contents[menu.pagepointer][i].name ~= "" then
			thename = menu.contents[menu.pagepointer][i].name	
		end
		if menu.contents[menu.pagepointer].menustyle == "msgbox" then
			v.drawString((65+thex)*i, 35+(10), thename, thecolor|V_ALLOWLOWERCASE)
		else
			v.drawString(65+thex, 35+(10*i), thename, thecolor|V_ALLOWLOWERCASE)
			if not (menu.contents[menu.pagepointer][i].slider or menu.contents[menu.pagepointer][i].flags & MNF_SLIDER) then
				local thevalue = menu.contents[menu.pagepointer][i].func and "" or menu.contents[menu.pagepointer][i].options[menu.contents[menu.pagepointer][i].value+1]
				if menu.contents[menu.pagepointer][i].flags & MNF_CHANGEPAGE then thevalue = "" end
				if menu.contents[menu.pagepointer][i].flags & MNF_SHOWVALUE then thevalue = menu.contents[menu.pagepointer][i].options[menu.contents[menu.pagepointer][i].value+1] end
				v.drawString(255, 35+(10*i), thevalue, thecolor, "right")
			elseif not menu.contents[menu.pagepointer][i].func then
				local patchs = v.cachePatch("M_SLIDEL")
				local patchm = v.cachePatch("M_SLIDEM")
				local patche = v.cachePatch("M_SLIDER")
				local patchc = v.cachePatch("M_SLIDEC")
				v.draw(213, 35+(10*i), patchs)
				for e = 1,8 do
					v.draw(215+(4*e), 35+(10*i), patchm)
				end
				v.draw(249, 35+(10*i), patche)
				local value = menu.contents[menu.pagepointer][i].value
				local maxvalue = menu.contents[menu.pagepointer][i].maxvalue-1
				v.drawScaled(FRACUNIT*215 + 36*FixedDiv(value, maxvalue), FRACUNIT*35 + (10*i)*FRACUNIT, FRACUNIT, patchc)
			end
		end
	end
	local patch = v.cachePatch("M_CURSOR")
	v.draw(45, 35+(10*menu.pointer), patch)
	local desctext = "No description found!"
	if menu.contents[menu.pagepointer][menu.pointer].description and menu.contents[menu.pagepointer][menu.pointer].description ~= "" then
		desctext = menu.contents[menu.pagepointer][menu.pointer].description
	end
	v.drawString(60, 160, desctext, V_ALLOWLOWERCASE, "thin")
end, "title")

--[[EXAMPLE FOR CHECKING A SETTING'S VALUE
	you have to check player.menu.contents[n1][n2].value, where n1 is the menu page, and n2 is the setting's number, this will return 1 when on and 0 when off, here's an example:
	local function myCoolFunction()
		if player.menu.contents[1][1].value
			--some cool code
		end
		return
	end
  	to check for multi-options setting's value, you do the same, do note that the value start from 0 while maxnumber "starts" from 1, so to check for the first value you check for 0, for the 2nd you check for 1 and so on
	local function myOtherFunction()
		if player.menu.contents[1][2].value < 5
			print("smol number")
		else
			print("beeg number")
		end
	end
]]