/*
	Model Viewer

	Used to display sprite models and animations with
	a free flying camera.

	(C) 2021-2022 by ashi
*/

addHook("ThinkFrame", function()
	if gamemap != 1001 then return end
end)

/*
-- freefly camera code
addHook("PlayerThink", function(player)
	if player.mdlviewcam.active == false then
		-- If we aren't in cammode then we need to keep
		-- the player's angle the same until we are.
		-- So that we don't get a desynched camera.
		player.mo.angle = player.mdlviewcam.angle
		return 
	end
	player.mdlviewcam.angle = player.mo.angle
end)
*/