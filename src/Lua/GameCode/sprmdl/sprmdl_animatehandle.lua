/* 
	Animation handler
	Welcome to the most complex part of the entire framework.

	This is where some of the most complex coding is placed.
	and also the component that requires the most amount of work
	done on the actual object itself.

	A ton of the code here is just dedicated to basic functionality
	that should be combined with your own code for your specific object.

	This is the most I can do for taking work off your shoulders.

	(C) 2021-2022 by ashi
*/

-- Linear following function.