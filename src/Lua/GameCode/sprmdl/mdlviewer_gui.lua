/*
	Model Viewer GUI

	all the GUI elements and debug components
	for the sprite model viewer

	Also handles taking the player in and out
	of camera mode.

	(C) 2021-2022 by ashi
*/

local cur_x, cur_y = 0, 0
local tab_count = 0

local click_block = false -- Used to prevent one click from constantly toggling things

-- Toolbar item tables
local top_tbar = {x = 0}
top_tbar[1] = {
	string = "Load Mdl",
	state = "idle"
}
top_tbar[2] = {
	string = "Animation",
	state = "idle"
}

/*
addHook("MapLoad", do
	if gamemap != 1001 then return end
	tab_count = 0
	for player in players.iterate do
		player.mdlviewcam.active = false
	end
end)
*/

local function mainui(v,p)
	if gamemap != 1001 then return end

	-- Top toolbar
	v.drawFill(0, 0, 520, 10, 83886094)
	v.drawString(320, 180, "SprMdl View v0.1", V_SNAPTORIGHT|V_SNAPTOBOTTOM|V_ALLOWLOWERCASE|V_50TRANS, "thin-right")
	
	top_tbar.x = 0
	-- Item loop
	for e = 1,#top_tbar do
		if (cur_y < 10 and (cur_x > top_tbar.x and cur_x < (top_tbar.x + v.stringWidth(top_tbar[e].string, V_ALLOWLOWERCASE, "thin")))) then
			-- This block handles menubar stuff
			if mouse.buttons == MB_BUTTON1 and click_block == false then
				click_block = true
				if top_tbar[e].state == "toggled" then
					top_tbar[e].state = "idle"
				else
					top_tbar[e].state = "toggled"
				end
			elseif top_tbar[e].state != "toggled" then
				top_tbar[e].state = "hover"
			end

			if mouse.buttons == 0 then
				click_block = false
			end
		elseif top_tbar[e].state != "toggled" then
			top_tbar[e].state = "idle"
		end

		if top_tbar[e].state == "toggled" or top_tbar[e].state == "hover" then
			v.drawFill(top_tbar.x, 0, v.stringWidth(top_tbar[e].string, V_ALLOWLOWERCASE, "thin"), 10, 83886231)
		end

		v.drawString(top_tbar.x, 2, top_tbar[e].string, V_SNAPTOLEFT|V_SNAPTOTOP|V_ALLOWLOWERCASE, "thin")
		top_tbar.x = $ + 2 + v.stringWidth(top_tbar[e].string, V_ALLOWLOWERCASE, "thin")
	end

	-- Bottom toolbar
	v.drawFill(0, 190, 520, 10, 100663310)
end

local function loadmobjdropdown(v,p)
	if gamemap != 1001 then return end

	if top_tbar[1].status == "toggled" then
		//v.drawFill(0, 10, 200, )
	end

end

local function cursor(v,p)
	if gamemap != 1001 then return end
	cur_x, cur_y = input.getCursorPosition()

	if cur_x != nil then
		v.drawScaled(cur_x*FU, cur_y*FU, FU/2, v.cachePatch("CURSOR"), V_SNAPTOLEFT|V_SNAPTOTOP)
	end
end

hud.add(mainui, "game")
hud.add(cursor, "game") -- Always draw the cursor last. This draws over everything

hud.disable("score")
hud.disable("time")
hud.disable("rings")
hud.disable("lives")