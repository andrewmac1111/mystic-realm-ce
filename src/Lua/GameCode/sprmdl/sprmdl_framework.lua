/*
	Sprite model framework

    Due to the more universal solution nature of this
    quite a bit of work still needs to be done inside 
    your own object but the most tedious parts of the 
    whole setup have been made much easier.

    This file should always be loaded first.

    All of the actual debug components are optional and
    can be removed for the final version of your mod.

	(C) 2021-2022 by ashi
*/
if sprmdl_framework == true then
    error("\131NOTICE:\128 Sprite Model Framework already loaded\nLoad Haulted")
end

-- Standard MT_SEGMENT used when mobj is missing.
freeslot("MT_SEGMENT", "S_SEGMENT")

mobjinfo[MT_SEGMENT] = {
    doomednum = -1,
    spawnstate = S_SEGMENT,
    radius = 24*FRACUNIT,
    mass = 8*FRACUNIT,
    flags = MF_NOCLIP|MF_NOBLOCKMAP|MF_NOGRAVITY
}

states[S_SEGMENT] = {
    sprite = SPR_UNKN,
    frame = 0,
    tics = -1,
    action = nil,
    nextstate = S_SEGMENT,
}

-- Model viewer variables
local model_mobj = {}
rawset(_G, "model_mobj", model_mobj)

/*
-- Define model
-- This function is used to prepare a model for setup.
-- So that we can summon the models without the actual
-- object coding for animation debugging within the model viewer.
local function freeslotmodel()
    print("Not implemented")
end
*/

-- Spawn handler
-- If mobj is nil we use a default MT_SEGMENT. which can be
-- customized pretty heavily to suit your needs after spawn.
--
-- I still recommend using a custom mobj so you can better control 
-- the actual function of each piece in a thinker if need be.
local function spawnobject(amount, group, parent, mobj)
    for i=1,amount do
        parent.obj_table.group[i] = P_SpawnMobjFromMobj(parent, 0, 0, 0, MT_SEGMENT)
        parent.obj_table.group[i].target = parent
        parent.obj_table.group[i].seg_num = i
    end
end


-- Mr. Worldwide with the global functions
rawset(_G, "MDL_spawnObject", spawnobject)


-- We made it through. Set a flag so that anything using the framework
-- Will know that the framework has already loaded. Incase it's using
-- an older version of the framework.
rawset(_G, "sprmdl_framework", true)
print("\
===========================================\
     Sprite Model Framework Version 0.1\
     Release date: NULL - INDEVELOPMENT\
\
              Created by Ashi\
===========================================\
")