/*
	save system debug

	This is the code used for the debug menus
	for the save/unlock system in mrce.

	ashi and Felix44
*/

local menu

local up1, up2 = 0
local down1, down2 = 0
local left1, left2 = 0
local right1, right2 = 0
local jump1, jump2 = 0
local spin1, spin2 = 0
local con1, con2 = 0
local c11, c12 = 0

local function notImplemented()
	print("Not Implemented")
end

local function closeMenu()
	menu.show = false
	debugmenu = false
end

local function eraseSlot()
	menu.show = false
	menu.datatest.show = true
	menu.datatest.erasemode = true
end

local function openDTest()
	menu.show = false
	menu.datatest.show = true
end

--MENU FLAGS
rawset(_G, "MNF_SLIDER", 1) --makes a setting display a slider instead of a string as its value (works the same way as the "slider" parameter)
rawset(_G, "MNF_NOSAVE", 2) --a value with this flag will not be saved/loaded from the save file
rawset(_G, "MNF_SHOWVALUE", 4) --forces the setting to display it's value, since only function settings and page changer settings don't display theirs it should only be used for those (NOTE: function settings don't have any option parameter set to them by default, don't forget to include it!)
rawset(_G, "MNF_CHANGEPAGE", 8) --settings with this flag will change the page pointer, allowing to make more complex menus with more pages and stuff, selcting these settings will change the menu page to the one defined in its "value" parameter, I suggest to give these settings the MNF_NOSAVE flag too since usually the "value" of this setting wouldn't be able to change and thus wouldn't need to be saved

addHook("PreThinkFrame", do
	if menu == nil then return end
	menu.timer = max($-1, 0)
end)


addHook("KeyDown", function(key)
	if menu == nil then return end
	if menu.show == true then
		local contents = menu.contents[menu.pagepointer]
		if menu.timer then 
			return true
		end
		if key.num == jump1
		or key.num == jump2 then
			if contents[menu.pointer].toggledby and contents[contents[menu.pointer].toggledby].value == 0 then return end
			if contents[menu.pointer].func then
				contents[menu.pointer].func()
			elseif contents[menu.pointer].flags & MNF_CHANGEPAGE then
				menu.pagename = contents[menu.pointer].pagename
				menu.pagepointer = contents[menu.pointer].value
				menu.pointer = 1
			else
				contents[menu.pointer].value = ($+1)%contents[menu.pointer].maxvalue
			end
			menu.timer = 5
			S_StartSound(nil, sfx_menu1)
			return true
		end
		if key.num == up1
		or key.num == up2 then
			menu.pointer = max($-1, 1)
			menu.timer = 5
			S_StartSound(nil, sfx_menu1)
			return true
		elseif key.num == down1
		or key.num == down2 then
			menu.pointer = min($+1, #contents)
			menu.timer = 5
			S_StartSound(nil, sfx_menu1)
			return true
		end
		if not key.num == con1
		or not key.num == con2 then
			return true
		end
	end
end)

addHook("MapLoad", do
	if gamemap == 784 then
		menu = {}
		menu.pointer = 1
		menu.pagepointer = 1
		menu.pagename = "Page 1"
		menu.show = false
		menu.timer = 0
		menu.contents = {
			[1] = {
				title = "SaveDebug";
				{value = 2, name = "SaveInfo", func= openDTest},
				{value = 1, name = "UnlockInfo", func = notImplemented},
				{value = 1, name = "EditData", func = notImplemented},
				{value = 2, name = "EraseData", flags = MNF_CHANGEPAGE|MNF_NOSAVE, pagename = "Page 2"},
				{value = 1, name = "Exit", func = closeMenu}
			},
			[2] = {
				title = "EraseData";
				{value = 1, name = "EraseSlot", func = eraseSlot},
				{value = 1, name = "Back", flags = MNF_CHANGEPAGE|MNF_NOSAVE, pagename = "Page 1"}
			}
		}
		menu.datatest = {}
		menu.datatest.show = false
		menu.datatest.slotnum = 1
		io.openlocal("client/MRCE/Name.dat", "a+"):close() --you can change "Name" to something else, change it for the line below too if you do
		local file = io.openlocal("client/MRCE/Name.dat", "r+")
		for e = 1,#menu.contents do
			for i = 1,#menu.contents[e] do
				if not menu.contents[e][i].flags then menu.contents[e][i].flags = 0 end
				if not menu.contents[e][i].selectedvflags then menu.contents[e][i].selectedvflags = V_YELLOWMAP end
				if not menu.contents[e][i].pagename then menu.contents[e][i].pagename = "Page "..menu.contents[e][i].value end
				if menu.contents[e][i].func then continue end
				if not menu.contents[e][i].maxvalue then
						menu.contents[e][i].maxvalue = 2
				elseif menu.contents[e][i].maxvalue == 1 then
					CONS_Printf(consoleplayer, "\133ERROR IN PAGE "..e.."\nSetting "..i.."'s maxvalue is 1! Are you sure it's intentional?")
					menu.contents[e][i].maxvalue = 2
					CONS_Printf(consoleplayer, "\133Maxvalue was set to 2.")
				elseif menu.contents[e][i].maxvalue < 1 then
					CONS_Printf(consoleplayer, "\133ERROR IN PAGE "..e.."\nSetting "..i.."'s maxvalue("..menu.contents[e][i].maxvalue..") is too small! Are you sure it's intentional?")
					menu.contents[e][i].maxvalue = 2
					CONS_Printf(consoleplayer, "\133Maxvalue was set to 2.")
				end
				if not menu.contents[e][i].options then
					menu.contents[e][i].options = {"OFF", "ON"}
				elseif #menu.contents[e][i].options < menu.contents[e][i].maxvalue and not (menu.contents[e][i].slider or menu.contents[e][i].flags & MNF_SLIDER) then
					CONS_Printf(consoleplayer, "\133ERROR IN PAGE "..e.."\nSetting "..i.."'s options can't be fewer than maxvalue("..menu.contents[e][i].maxvalue..")!")
					for a =1,menu.contents[e][i].maxvalue do
						if menu.contents[e][i].options[a] then continue end
						menu.contents[e][i].options[a] = "Missing!"
					end
				end
				if menu.contents[e][i].flags & MNF_NOSAVE then continue end
				local text = file:read("*l")
				if text then
					menu.contents[e][i].value = tonumber(text)	
				end
			end
		end
		up1, up2 = input.gameControlToKeyNum(GC_FORWARD)
		down1, down2 = input.gameControlToKeyNum(GC_BACKWARD)
		left1, left2 = input.gameControlToKeyNum(GC_STRAFELEFT)
		right1, right2 = input.gameControlToKeyNum(GC_STRAFERIGHT)
		jump1, jump2 = input.gameControlToKeyNum(GC_JUMP)
		spin1, spin2 = input.gameControlToKeyNum(GC_SPIN)
		con1, con2 = input.gameControlToKeyNum(GC_CONSOLE)
		c11, c12 = input.gameControlToKeyNum(GC_CUSTOM1)
	end
end)

COM_AddCommand("saveDebug", function(player) --you can change the command's name to anything you want
	menu.show = not $
	debugmenu = true
	if menu.show == false then
		local file = io.openlocal("client/MRCE/Name.dat", "w") --if you changed "Name" in the other lines, change it here too!
		for e = 1,#menu.contents do
			for i = 1,#menu.contents[e] do
				if menu.contents[e][i].func or menu.contents[e][i].flags & MNF_NOSAVE then continue end
				file:write(menu.contents[e][i].value.."\n")
			end
		end
		file:close()
	end
end, COM_LOCAL)

addHook("KeyDown", function(key)
	if menu.datatest.show == true then
		if menu.timer then 
			return true
		end
		local dat = menu.datatest
		if dat.delslot == true then
			if key.num == right1
			or key.num == right2 
			or key.num == left1
			or key.num == left2 then
				dat.delopt = not $
				return true
			end
			if key.num == jump1
			or key.num == jump2 then
				if dat.delopt == false then
					dat.delslot = false
				else
					DAT_DeleteSaveFile(dat.slotnum)
					dat.delslot = false
				end
				return true
			end
			return true
		end
		if key.num == spin1
		or key.num == spin2 then
			dat.show = false
			menu.show = true
			menu.timer = 5
			return true
		end
		if key.num == left1
		or key.num == left2 then
			if dat.slotnum != 1 then
				dat.slotnum = $ - 1
				menu.timer = 5
				return true
			end
			return true
		elseif key.num == right1
		or key.num == right2 then
			dat.slotnum = $ + 1
			menu.timer = 5
			return true
		end
		if key.num == c11
		or key.num == c12 then
			dat.delslot = true
			dat.delopt = true
		end
		return true
	end
end)

hud.add(function(v,p)
	if not menu.datatest.show then return end
	local dat = menu.datatest
	-- Drawing the BG, title, and control hint
	v.drawFill(0,0,320,200,253)
	v.drawString(160,0,"Data Test Mode",0,"center")
	v.drawString(0, 190, "(Spin): Exit  (Jump): Load Slot  (C1): Delete Slot", V_TRANSLUCENT, "thin")

	-- Draw the slot number. So we know what we are viewing
	v.drawString(160, 180, tostring(menu.datatest.slotnum))

	-- If we've changed the slot we should reload said slot.
	-- Just so we see live updates on save changes.
	if menu.datatest.prevslot == nil
	or menu.datatest.prevslot != menu.datatest.slotnum then
		menu.datatest.prevslot = menu.datatest.slotnum
		DAT_LoadSaveFile(dat.slotnum)
	end

	-- Drawing all the relevent slot info as long as the slot isn't nil
	if dat_saveinfo[dat.slotnum] == nil then
		-- Retry loading incase we just never loaded the slot
		DAT_LoadSaveFile(dat.slotnum)
		v.drawString(160, 100, "NO SAVE DATA IN SLOT", V_REDMAP, "center")
	else
		-- Draw what character was used in this slot
		local cssgfx = v.getSprite2Patch(dat_saveinfo[dat.slotnum].skinname, SPR2_XTRA, false, B)
		if cssgfx == nil then
			v.drawString(10, 10, "Character not loaded!", V_REDMAP, "thin")
		else
			v.drawScaled(10*FU, 10*FU, FU/2, cssgfx, 0)
			v.drawString(10, 10, "Character:")
			v.drawString(10, 20, skins[dat_saveinfo[dat.slotnum].skinname].realname)
		end

		-- Draw the map picture and mapname
		local mapgfx = IntToExtMapNum(tonumber(dat_saveinfo[dat.slotnum].mapnumber)) + "P"
		if v.patchExists(mapgfx) then
			v.draw(150, 10, v.cachePatch(mapgfx))
		else
			v.draw(150, 10, v.cachePatch("BLANKLVL"))
		end
		v.drawString(150, 10, "Map:")
		if mapheaderinfo[tonumber(dat_saveinfo[dat.slotnum].mapnumber)] then
			v.drawString(150, 20, mapheaderinfo[tonumber(dat_saveinfo[dat.slotnum].mapnumber)].lvlttl)
			v.drawString(
				150+v.stringWidth(mapheaderinfo[tonumber(dat_saveinfo[dat.slotnum].mapnumber)].lvlttl) + 5,
				20, mapheaderinfo[tonumber(dat_saveinfo[dat.slotnum].mapnumber)].actnum
			)
		else
			v.drawString(150, 20, "Map is not loaded!", V_REDMAP, "thin")
		end

		-- Draw the emeralds (I have to do this)
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD1) == 1) then
			v.draw(10, 100, v.cachePatch("CHAOS1"))
		end
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD2) == 2) then
			v.draw(30, 100, v.cachePatch("CHAOS2"))
		end
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD3) == 4) then
			v.draw(50, 100, v.cachePatch("CHAOS3"))
		end
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD4) == 8) then
			v.draw(70, 100, v.cachePatch("CHAOS4"))
		end
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD5) == 16) then
			v.draw(90, 100, v.cachePatch("CHAOS5"))
		end
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD6) == 32) then
			v.draw(110, 100, v.cachePatch("CHAOS6"))
		end
		if ((tonumber(dat_saveinfo[dat.slotnum].emeralds) & EMERALD7) == 64) then
			v.draw(130, 100, v.cachePatch("CHAOS7"))
		end
		v.drawString(10, 120, "Episode Completed:"..dat_saveinfo[dat.slotnum].gamecomplete)
		-- MRCE specific line for second quest unlockable
		if dat_saveinfo[dat.slotnum].gamecomplete == "true" then
			v.drawString(10, 130, "Second Quest is avaliable!", V_YELLOWMAP)
		end
	end
	if menu.datatest.delslot == true then
		v.drawFill(60, 25, 200, 45, 254)
		v.drawString(160, 25, "Delete Save Slot?", V_ALLOWLOWERCASE, "center")
		if menu.datatest.delopt == true then
			v.drawString(70, 40, "No", V_ALLOWLOWERCASE)
			v.drawString(100, 40, "Yes", V_ALLOWLOWERCASE|V_YELLOWMAP)
		else
			v.drawString(70, 40, "No", V_ALLOWLOWERCASE|V_YELLOWMAP)
			v.drawString(100, 40, "Yes", V_ALLOWLOWERCASE)
		end
	end
end, "title")

hud.add(function(v,player) --this draws the menu on the screen, you can change the values to what you prefer if you know what you are doing
	if not menu.show then return end
	local color = 253
	v.drawFill(60, 25, 200, 25+(10*#menu.contents[menu.pagepointer]), color)
	v.drawString(160, 30, menu.contents[menu.pagepointer].title, 0, "center")
	local menutype = nil
	for i = 1,#menu.contents[menu.pagepointer] do
		local thecolor = menu.contents[menu.pagepointer][i].vflags or 0
		local thex = 0
		if menu.pointer == i then thecolor = menu.contents[menu.pagepointer][i].selectedvflags end
		if menu.contents[menu.pagepointer][i].toggledby then
			thex = 10
			if menu.contents[menu.pagepointer][menu.contents[menu.pagepointer][i].toggledby].value == 0 then
				thecolor = $|V_TRANSLUCENT
			end
		end
		local thename = "Missing name!"
		if menu.contents[menu.pagepointer][i].name and menu.contents[menu.pagepointer][i].name ~= "" then
			thename = menu.contents[menu.pagepointer][i].name	
		end
		v.drawString(65+thex, 35+(10*i), thename, thecolor|V_ALLOWLOWERCASE)
		if not (menu.contents[menu.pagepointer][i].slider or menu.contents[menu.pagepointer][i].flags & MNF_SLIDER) then
			local thevalue = menu.contents[menu.pagepointer][i].func and "" or menu.contents[menu.pagepointer][i].options[menu.contents[menu.pagepointer][i].value+1]
			if menu.contents[menu.pagepointer][i].flags & MNF_CHANGEPAGE then thevalue = "" end
			if menu.contents[menu.pagepointer][i].flags & MNF_SHOWVALUE then thevalue = menu.contents[menu.pagepointer][i].options[menu.contents[menu.pagepointer][i].value+1] end
			v.drawString(255, 35+(10*i), thevalue, thecolor, "right")
		end
	end
	local patch = v.cachePatch("M_CURSOR")
	v.draw(45, 35+(10*menu.pointer), patch)
end, "title")