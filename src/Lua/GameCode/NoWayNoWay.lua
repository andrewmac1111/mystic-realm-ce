/*
	MRCE No Way? No Way! Error screen

	Used when a version lower than the minimum supported
	version is detected

	Screen uses the MRCE main menu message box format.
	Just thought it'd be a nice touch

	MRCE does not work on anything lower than 2.2.10
	due to extensive use of new functions in 2.2.10 in
	- Egg Animus DWZ
	- AGZ's boss and some enemies
	- Egg Decker
	- Menu
	- Credits
	- Intermission
	- HUD Elements
	you get the idea

	You're only presented with an option to quit the game.
	As that's really all you can do at this point
	which can also be done from the pause menu.

	(C) 2022 by ashi
*/

local x = -520
local animation_timer = 0

addHook("MapLoad", do
	if gamemap != 99 then
		-- Force warp to map99 because of how
		-- broken MRCE will be without 2.2.10 functions
		COM_BufInsertText(consoleplayer, "map map99 -f")
	end
	if gamemap == 99 then
		S_ChangeMusic("AGZALT")
	end
end)

local function noway_screen(v)
	if gamemap != 99 then return end

	-- Very unfinished due to me not having everything
	-- for the regular menu message boxes setup
	-- it's very primitive but it does the job

	-- Draw background
	v.drawFill(0,0,320,200,253)
	v.drawScaled(0,0,FU/4,v.cachePatch("NWBACK"))

	-- Draw message box background graphic
	v.drawScaled(x*FU, 10*FU, FU/3, v.cachePatch("MSGBACK"))

	-- Draw all of our message box contents
	-- Noway header graphic
	v.drawString(160,90,"Unfortunately. Due to heavy use of 2.2.10 specific functions MRCE",V_ALLOWLOWERCASE,"thin-center")
	v.drawString(160,100,"will not function on any versions lower than 2.2.10",V_ALLOWLOWERCASE,"thin-center")
	v.drawString(160,110,"Please upgrade your game to version 2.2.10 if possible.",V_ALLOWLOWERCASE,"thin-center")
	-- Draw "Close game button"
	v.drawString(160,140,"Quit",V_GREENMAP,"center")

	-- animation ticker
	if x < -150 then
		x = $ + 26
	end
end

addHook("PlayerThink", function(player)
	if gamemap != 99 then return end
	if player.cmd.buttons & BT_JUMP 
	and animation_timer != "quit" then
		-- Perform the dialog close animation
		-- and run the quit command
		animation_timer = "quit"
	end
	if animation_timer == "quit" then
		if x < 600
		and x > -150 then
			x = $ + 26
		else
			COM_BufInsertText(consoleplayer, "quit")
		end
	end
end)

hud.add(noway_screen,"game")
hud.add(noway_screen,"scores")
