//Wow this mods been passed around a lot. Originally based on hyper abilities v4.1.2, then edited for mrce. 
//It barely maintains any of the original code, but credit is still due.  v4.1.2 by GameBoyTM101
//Credits for the original scripts from 2.1 go to MotdSpork on the SRB2 Message Board, as well as HitKid61/HitCoder
//Additional credits to Radicalicious for the Multiglide Knuckles and Infinite Flight Tails code.
//Also credit to DirkTheHusky for the custom colors themed after the 7 emeralds

--this is here for mrce to detect if hyper has been unlocked
rawset(_G, "mrce_hyperunlocked", false)

freeslot("SKINCOLOR_GRAPE", "SKINCOLOR_GALAXY")

local luabanks = reserveLuabanks()

//DirkTheHusky was here.

skincolors[SKINCOLOR_GRAPE] = {
	name = "Grape",
	ramp = {178,162,163,165,166,167,167,157,158,158,159,253,253,30,30,31},
	invcolor = SKINCOLOR_SUNSET,
	invshade = 4,
	chatcolor = V_PURPLEMAP,
	accessible = true
}

skincolors[SKINCOLOR_GALAXY] = {
	name = "Galaxy",
	ramp = {179,180,183,185,186,169,169,253,253,253,254,254,254,254,31,31},
	invcolor = SKINCOLOR_SKY,
	invshade = 3,
	chatcolor = V_PURPLEMAP,
	accessible = true
}

//when you have sensitive eyes, this command allows to disable screen flashing
COM_AddCommand("hyperflash", function(player, arg)
	if arg then
		if arg == "1" or arg == "true" or arg == "on" then
			if io and player == consoleplayer then
				local file = io.openlocal("client/mrce/hyperflash.dat", "w+")
				file:write(arg)
				file:close()
			end
			player.screenflash = true
			CONS_Printf(player, "Hyper form screen flashing has been enabled.")
		elseif arg == "0" or arg == "false" or arg == "off" then
			if io and player == consoleplayer then
				local file = io.openlocal("client/mrce/hyperflash.dat", "w+")
				file:write(arg)
				file:close()
			end	
			player.screenflash = false
			CONS_Printf(player, "Hyper form screen flashing has been disabled.")
		elseif player.screenflash then
			CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently on.")
		else
			CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently off.")
		end
	elseif player.screenflash then
		CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently on.")
	else
		CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently off.")
	end
end, 1)

addHook("PlayerSpawn", function(p)
	if io and p == consoleplayer and p.screenflash == nil then
		local file = io.openlocal("client/mrce/hyperflash.dat")
		if file then
			local string = file:read("*a")
			if string == "1" or string == "true" or string == "on" then
				COM_BufInsertText(p, "hyperflash on")
			elseif string == "0" or string == "false" or string == "off" or string == nil then
				COM_BufInsertText(p, "hyperflash off")
			end
			file:close()
		end
	end
end)

//handle unlocking hyper, also gives knux and tails super abilities
addHook("PreThinkFrame",do
    for p in players.iterate do
        if not (p and p.mo and p.mo.valid) then continue end
        if not p.powers[pw_super] and p.mo.skin == "knuckles" and mrce_hyperunlocked == true then
            p.charflags = $ & ~SF_MULTIABILITY
        end
        if p.powers[pw_super] and p.mo.skin == "knuckles" and mrce_hyperunlocked == true then
            p.charflags = $1 | SF_MULTIABILITY
        end
        if p.powers[pw_super] and p.pflags & PF_THOKKED then
            local ca = p.charability
            if ca == CA_FLY or ca == CA_SWIM then
                p.powers[pw_tailsfly] = 8*TICRATE
            end
        elseif p.powers[pw_super] then
            p.powers[pw_tailsfly] = 0
        end
		if (gamemap == 130 and (p.pflags & PF_FINISHED))
		and All7Emeralds(emeralds) and mrce_hyperunlocked == false then
			S_StartSound(null, sfx_s3k9c)
			luabanks[0] = 1
			mrce_hyperunlocked = true
		end
		if luabanks[0] == 1
		or p.hypercheat == true then
			mrce_hyperunlocked = true
		else mrce_hyperunlocked = false
		end
    end
end)

addHook("AbilitySpecial", function(player)
	if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "supersonic")
	and not (player.pflags & PF_THOKKED)
	and G_PlatformGametype()
	and player.hypermode == 1 then// and gamemap != 136
		P_NukeEnemies(player.mo, player.mo, 2048*FRACUNIT)
		if player.screenflash == true then
			P_FlashPal(player, PAL_WHITE, 7)
		end
		S_StartSound(player.mo, sfx_zoom)
	end
end)

addHook("ThinkFrame", do
	for player in players.iterate do
		if player.mo.skin == "supersonic" then
			player.charability = 18
			if gamemap == 98 or gamemap == 99 then
				if not (leveltime%35)
				and not (player.bot)
				and not (player.mo.state >= S_PLAY_SUPER_TRANS1) and (player.mo.state <= S_PLAY_SUPER_TRANS6) then
					P_GivePlayerRings(player, 1)
				end
			end
		end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic") then
			player.mrce_hyperimages = true
		end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce_canhyper == true) then
			if player.powers[pw_super] then
				if not player.mo.hypersparklocation then
					player.mo.hypersparklocation = 0
				end
				player.mo.hypersparklocation = $1 + 14*FRACUNIT
			
				if not player.mo.hyperflashcolor then
					player.mo.hyperflashcolor = 0
				end
			
				player.mo.hyperflashcolor = $1+1
			
				if player.mo.hyperflashcolor > 59 then
					player.mo.hyperflashcolor = 1
				end
			end
		end
	end
end)

//Super sparks made by HitKid61/HitCoder: Thank you :)
addHook("ThinkFrame", do
	for player in players.iterate do
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce_canhyper == true) then
			
//			if player.custom1down == nil
//				player.custom1down = 0
//			end
			if not (player.hypermode)
			or player.powers[pw_super] == 0
			or player.exiting
			or player.mo.health == 0 then
				player.hypermode = 0
			end
//			if P_IsObjectOnGround(player.mo)
//			and player.cmd.buttons & BT_CUSTOM1
//			and player.hypermode == 0
//				player.custom1down = 1
//			end
//			if not (player.cmd.buttons & BT_CUSTOM1)
//				player.custom1down = 0
//			end
//			if player.mo.state >= S_PLAY_SUPER_TRANS1
//			and player.mo.state <= S_PLAY_SUPER_TRANS6
//				player.mo.momz = 0
//				player.powers[pw_nocontrol] = 1
//			end
			if player.powers[pw_super]
//			and player.cmd.buttons & BT_CUSTOM1
//			and player.custom1down == 0
			and player.mo.health
			and player.hypermode == 0
			and mrce_hyperunlocked == true then
			--and not (player.pflags & PF_THOKKED)
				player.pflags = $1|PF_THOKKED
				player.hypermode = 1
//				player.custom1down = 1
//				player.mo.state = S_PLAY_SUPER_TRANS1
				S_StartSound(player.mo, sfx_supert)
			end
//			if  player.cmd.buttons & BT_CUSTOM1
//			and player.custom1down == 0
//			and player.powers[pw_super] 
//			and player.hypermode == 1
//			and not (player.pflags & PF_THOKKED)
//				player.pflags = $1|PF_THOKKED
//				player.hypermode = 0
//				S_StartSound(player.mo, sfx_s3k66)
//				player.custom1down = 1
//			end
			

			if player.powers[pw_super]
			and player.hypermode == 1
			and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce_canhyper == true) then
				player.powers[pw_underwater] = 0
				player.powers[pw_spacetime] = 0

				/*local hyperparticle = P_SpawnMobjFromMobj(player.mo, P_RandomRange(-10, 10) * FRACUNIT, P_RandomRange(-10, 10) * FRACUNIT, 20*FRACUNIT, MT_SUPERSPARK) 
				hyperparticle.momx = P_RandomRange(-9, 9) * FRACUNIT
				hyperparticle.momy = P_RandomRange(-9, 9) * FRACUNIT
				hyperparticle.momz =  P_RandomRange(-9, 9) * FRACUNIT
				hyperparticle.colorized = true
				if P_RandomRange(1, 2) == 1
					hyperparticle.color = SKINCOLOR_YELLOW
				else
					hyperparticle.color = SKINCOLOR_CYAN
				end
				hyperparticle.fuse = 10
				hyperparticle.scale = player.mo.scale *2/3
				hyperparticle.source = player.mo*/

				if player.mo.hyperflashcolor < 5
				or player.mo.hyperflashcolor == 69 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
			
				if player.mo.hyperflashcolor > 4
				and player.mo.hyperflashcolor < 7 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end

				if player.mo.hyperflashcolor > 6
				and player.mo.hyperflashcolor < 9 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end

				if player.mo.hyperflashcolor > 8
				and player.mo.hyperflashcolor < 11 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 10
				and player.mo.hyperflashcolor < 13 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 12
				and player.mo.hyperflashcolor < 15 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 14
				and player.mo.hyperflashcolor < 17 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 16
				and player.mo.hyperflashcolor < 19 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 18
				and player.mo.hyperflashcolor < 21 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 20
				and player.mo.hyperflashcolor < 23 then
					player.mo.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 22
				and player.mo.hyperflashcolor < 25 then
					player.mo.color = SKINCOLOR_SUPERMINT2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 24
				and player.mo.hyperflashcolor < 27 then
					player.mo.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 26
				and player.mo.hyperflashcolor < 29 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 28
				and player.mo.hyperflashcolor < 31 then
					player.mo.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 30
				and player.mo.hyperflashcolor < 33 then
					player.mo.color = SKINCOLOR_SUPERRUBY2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 32
				and player.mo.hyperflashcolor < 35 then
					player.mo.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 34
				and player.mo.hyperflashcolor < 37 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 36
				and player.mo.hyperflashcolor < 39 then
					player.mo.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 38
				and player.mo.hyperflashcolor < 41 then
					player.mo.color = SKINCOLOR_SUPERWAVE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 40
				and player.mo.hyperflashcolor < 43 then
					player.mo.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 42
				and player.mo.hyperflashcolor < 45 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 44
				and player.mo.hyperflashcolor < 47 then
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 46
				and player.mo.hyperflashcolor < 49 then
					player.mo.color = SKINCOLOR_SUPERCOPPER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 48
				and player.mo.hyperflashcolor < 51 then
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 50
				and player.mo.hyperflashcolor < 53 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
	
				if player.mo.hyperflashcolor > 52
				and player.mo.hyperflashcolor < 55 then
					player.mo.hyperflashcolor = 1
					player.mo.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
	
				if player.mo.hyperflashcolor > 54
				and player.mo.hyperflashcolor < 57 then
					player.mo.color = SKINCOLOR_SUPERAETHER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
	
				if player.mo.hyperflashcolor > 56
				and player.mo.hyperflashcolor < 59 then
					player.mo.hyperflashcolor = 1
					player.mo.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
			end
		end
		if player.mo then
			if player.powers[pw_super] and player.mrce_ultrastar == true and mrce_hyperunlocked == true then
			
				if not player.mo.ultraflashcolor then
					player.mo.ultraflashcolor = 0
				end
			
				player.mo.ultraflashcolor = $1+1
			
				if player.mo.ultraflashcolor > 139 then
					player.mo.ultraflashcolor = 1
				end
			end
		end
		if player.mo and player.mrce_ultrastar == true and mrce_hyperunlocked == true then
			if not (player.hypermode)
			or player.powers[pw_super] == 0
			or player.exiting
			or player.mo.health == 0 then
				player.hypermode = 0
			end		

			if player.powers[pw_super] and player.mrce_ultrastar == true and mrce_hyperunlocked == true then
				if player.mo.ultraflashcolor < 2
				or player.mo.ultraflashcolor == 143 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 1
				and player.mo.ultraflashcolor < 4 then
					player.mo.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 3
				and player.mo.ultraflashcolor < 6 then
					player.mo.color = SKINCOLOR_SUPERMINT2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 5
				and player.mo.ultraflashcolor < 8 then
					player.mo.color = SKINCOLOR_SUPERMINT3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 7
				and player.mo.ultraflashcolor < 10 then
					player.mo.color = SKINCOLOR_SUPERMINT4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 9
				and player.mo.ultraflashcolor < 12 then
					player.mo.color = SKINCOLOR_SUPERMINT5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 11
				and player.mo.ultraflashcolor < 14 then
					player.mo.color = SKINCOLOR_SUPERMINT4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 13
				and player.mo.ultraflashcolor < 16 then
					player.mo.color = SKINCOLOR_SUPERMINT3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 15
				and player.mo.ultraflashcolor < 18 then
					player.mo.color = SKINCOLOR_SUPERMINT2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 17
				and player.mo.ultraflashcolor < 20 then
					player.mo.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 19
				and player.mo.ultraflashcolor < 22 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 21
				and player.mo.ultraflashcolor < 24 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 23
				and player.mo.ultraflashcolor < 26 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 25
				and player.mo.ultraflashcolor < 28 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 27
				and player.mo.ultraflashcolor < 30 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 29
				and player.mo.ultraflashcolor < 32 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 31
				and player.mo.ultraflashcolor < 34 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 33
				and player.mo.ultraflashcolor < 36 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 35
				and player.mo.ultraflashcolor < 38 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 37
				and player.mo.ultraflashcolor < 40 then
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 39
				and player.mo.ultraflashcolor < 42 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 41
				and player.mo.ultraflashcolor < 44 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 43
				and player.mo.ultraflashcolor < 46 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 45
				and player.mo.ultraflashcolor < 48 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 47
				and player.mo.ultraflashcolor < 50 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 49
				and player.mo.ultraflashcolor < 52 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 51
				and player.mo.ultraflashcolor < 54 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 53
				and player.mo.ultraflashcolor < 56 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 55
				and player.mo.ultraflashcolor < 58 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 57
				and player.mo.ultraflashcolor < 60 then
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 59
				and player.mo.ultraflashcolor < 62 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 61
				and player.mo.ultraflashcolor < 64 then
					player.mo.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 63
				and player.mo.ultraflashcolor < 66 then
					player.mo.color = SKINCOLOR_SUPERWAVE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 65
				and player.mo.ultraflashcolor < 68 then
					player.mo.color = SKINCOLOR_SUPERWAVE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 67
				and player.mo.ultraflashcolor < 70 then
					player.mo.color = SKINCOLOR_SUPERWAVE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 69
				and player.mo.ultraflashcolor < 72 then
					player.mo.color = SKINCOLOR_SUPERWAVE5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 71
				and player.mo.ultraflashcolor < 74 then
					player.mo.color = SKINCOLOR_SUPERWAVE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 73
				and player.mo.ultraflashcolor < 76 then
					player.mo.color = SKINCOLOR_SUPERWAVE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 75
				and player.mo.ultraflashcolor < 78 then
					player.mo.color = SKINCOLOR_SUPERWAVE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 77
				and player.mo.ultraflashcolor < 80 then
					player.mo.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 79
				and player.mo.ultraflashcolor < 82 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 81
				and player.mo.ultraflashcolor < 84 then
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 83
				and player.mo.ultraflashcolor < 86 then
					player.mo.color = SKINCOLOR_SUPERCOPPER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 85
				and player.mo.ultraflashcolor < 88 then
					player.mo.color = SKINCOLOR_SUPERCOPPER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 87
				and player.mo.ultraflashcolor < 90 then
					player.mo.color = SKINCOLOR_SUPERCOPPER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 89
				and player.mo.ultraflashcolor < 92 then
					player.mo.color = SKINCOLOR_SUPERCOPPER5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 91
				and player.mo.ultraflashcolor < 94 then
					player.mo.color = SKINCOLOR_SUPERCOPPER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 93
				and player.mo.ultraflashcolor < 96 then
					player.mo.color = SKINCOLOR_SUPERCOPPER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 95
				and player.mo.ultraflashcolor < 98 then
					player.mo.color = SKINCOLOR_SUPERCOPPER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 97
				and player.mo.ultraflashcolor < 100 then
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 99
				and player.mo.ultraflashcolor < 102 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 101
				and player.mo.ultraflashcolor < 104 then
					player.mo.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 103
				and player.mo.ultraflashcolor < 106 then
					player.mo.color = SKINCOLOR_SUPERRUBY2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 105
				and player.mo.ultraflashcolor < 108 then
					player.mo.color = SKINCOLOR_SUPERRUBY3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 107
				and player.mo.ultraflashcolor < 110 then
					player.mo.color = SKINCOLOR_SUPERRUBY4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 109
				and player.mo.ultraflashcolor < 112 then
					player.mo.color = SKINCOLOR_SUPERRUBY5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 111
				and player.mo.ultraflashcolor < 114 then
					player.mo.color = SKINCOLOR_SUPERRUBY4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 113
				and player.mo.ultraflashcolor < 116 then
					player.mo.color = SKINCOLOR_SUPERRUBY3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 115
				and player.mo.ultraflashcolor < 118 then
					player.mo.color = SKINCOLOR_SUPERRUBY2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 117
				and player.mo.ultraflashcolor < 120 then
					player.mo.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 119
				and player.mo.ultraflashcolor < 122 then
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 121
				and player.mo.ultraflashcolor < 124 then
					player.mo.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 123
				and player.mo.ultraflashcolor < 126 then
					player.mo.color = SKINCOLOR_SUPERAETHER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 125
				and player.mo.ultraflashcolor < 128 then
					player.mo.color = SKINCOLOR_SUPERAETHER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 127
				and player.mo.ultraflashcolor < 130 then
					player.mo.color = SKINCOLOR_SUPERAETHER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 129
				and player.mo.ultraflashcolor < 132 then
					player.mo.color = SKINCOLOR_SUPERAETHER5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 131
				and player.mo.ultraflashcolor < 134 then
					player.mo.color = SKINCOLOR_SUPERAETHER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 133
				and player.mo.ultraflashcolor < 136 then
					player.mo.color = SKINCOLOR_SUPERAETHER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 135
				and player.mo.ultraflashcolor < 138 then
					player.mo.color = SKINCOLOR_SUPERAETHER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 137
				and player.mo.ultraflashcolor < 140 then
					player.mo.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 139
				and player.mo.ultraflashcolor < 142 then
					player.mo.color = SKINCOLOR_BLANK
					player.mo.ultraflashcolor = 0
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			end
		end
	end
end)

addHook("PlayerThink", function(p)
	if p.spectator then return false end
	if not p.realmo then return false end
	p.mrce_canhyper = $ or false
	p.mrce_hyperimages = $ or false
	p.mrce_ultrastar = $ or false
	if p.mo.skin == "tails" and (mrce_hyperunlocked == true or gamemap == 136) then
		p.charflags = $1 | SF_SUPER
	elseif p.mo.skin == "tails" and mrce_hyperunlocked == false then
		p.charflags =  $ & ~SF_SUPER
	end
	if p.powers[pw_super] and p.pflags & PF_THOKKED and p.mo.skin == "tails" then
		p.powers[pw_tailsfly] = 8*TICRATE
	end
	if p.powers[pw_super] and mrce_hyperunlocked == true then
		if p.superboost == nil then
			p.superboost = false
			p.boosting = false
		end
		if p.mo.skin == "modernsonic" then
			if p.superboost == true then
				if (leveltime % 5 == 0) then  
				p.rings = p.rings + 1
				end
			end
			--also nukes because why the fuck not lol
			if (p.boosting == true or p.superboost == true) and gamemap != 136 then
				if p.modernnuker == true then
					P_NukeEnemies(p.mo, p.mo, 600*FRACUNIT)
					if p.screenflash == true then
						P_FlashPal(p, PAL_WHITE, 7)
					end
					p.modernnuker = false
				end
			else
				p.modernnuker = true
			end
		end
	end
	if p.mo.hyperflashcolor == nil then
		 p.mo.hyperflashcolor = 1
	 end
	if (p.powers[pw_super] and (p.speed >= 8*FRACUNIT or p.mo.momz >= 3*FRACUNIT or p.mo.momz <= -3*FRACUNIT) and p.mrce_hyperimages == true)
	and mrce_hyperunlocked == true
	and not p.mo.state == ((p.mo.state == S_PLAY_SUPER_TRANS1) or (p.mo.state == S_PLAY_SUPER_TRANS2) or (p.mo.state == S_PLAY_SUPER_TRANS3) or (p.mo.state == S_PLAY_SUPER_TRANS4) or (p.mo.state == S_PLAY_SUPER_TRANS5) or (p.mo.state == S_PLAY_SUPER_TRANS6)) then
		local AfterImageSpawn = P_SpawnGhostMobj(p.mo)
		AfterImageSpawn.colorized = true
		AfterImageSpawn.fuse = 4
		if p.mo.hyperflashcolor < 5
		or p.mo.hyperflashcolor == 69 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 4
		and p.mo.hyperflashcolor < 7 then
			AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE1
		end
		
		if p.mo.hyperflashcolor > 6
		and p.mo.hyperflashcolor < 9 then
		AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE2
		end
		
		if p.mo.hyperflashcolor > 8
		and p.mo.hyperflashcolor < 11 then
			AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE1
		end
		
		if p.mo.hyperflashcolor > 10
		and p.mo.hyperflashcolor < 13 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 12
		and p.mo.hyperflashcolor < 15 then
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM1
		end
		
		if p.mo.hyperflashcolor > 14
		and p.mo.hyperflashcolor < 17 then
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM2
		end
		
		if p.mo.hyperflashcolor > 16
		and p.mo.hyperflashcolor < 19 then
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM1
		end
		
		if p.mo.hyperflashcolor > 18
		and p.mo.hyperflashcolor < 21 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 20
		and p.mo.hyperflashcolor < 23 then
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT1
		end
		
		if p.mo.hyperflashcolor > 22
		and p.mo.hyperflashcolor < 25 then
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT2
		end
		
		if p.mo.hyperflashcolor > 24
		and p.mo.hyperflashcolor < 27 then
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT1
		end
		
		if p.mo.hyperflashcolor > 26
		and p.mo.hyperflashcolor < 29 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 28
		and p.mo.hyperflashcolor < 31 then
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY1
		end
		
		if p.mo.hyperflashcolor > 30
		and p.mo.hyperflashcolor < 33 then
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY2
		end
		
		if p.mo.hyperflashcolor > 32
		and p.mo.hyperflashcolor < 35 then
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY1
		end
		
		if p.mo.hyperflashcolor > 34
		and p.mo.hyperflashcolor < 37 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 36
		and p.mo.hyperflashcolor < 39 then
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE1
		end
		
		if p.mo.hyperflashcolor > 38
		and p.mo.hyperflashcolor < 41 then
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE2
		end
		
		if p.mo.hyperflashcolor > 40
		and p.mo.hyperflashcolor < 43 then
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE1
		end
		
		if p.mo.hyperflashcolor > 42
		and p.mo.hyperflashcolor < 45 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 44
		and p.mo.hyperflashcolor < 47 then
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER1
		end
		
		if p.mo.hyperflashcolor > 46
		and p.mo.hyperflashcolor < 49 then
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER2
		end
		
		if p.mo.hyperflashcolor > 48
		and p.mo.hyperflashcolor < 51 then
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER1
		end
		
		if p.mo.hyperflashcolor > 50
		and p.mo.hyperflashcolor < 53 then
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 52
		and p.mo.hyperflashcolor < 55 then
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER1
		end
		
		if p.mo.hyperflashcolor > 54
		and p.mo.hyperflashcolor < 57 then
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER2
		end
		
		if p.mo.hyperflashcolor > 56
		and p.mo.hyperflashcolor < 59 then
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER1
		end
	end
end)