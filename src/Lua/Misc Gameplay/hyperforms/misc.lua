addHook("TouchSpecial", function(mo, toucher)
  if toucher.player.hypermode == 1 then return true -- hyper forms don't need bubbles
  else toucher.player.powers[pw_spacetime] = 12 * TICRATE end --midnight freeze's ice water uses space countdown. put the function here to save space
end, MT_EXTRALARGEBUBBLE)

local function Break_shit (plr, thing)
	if (thing.type == MT_SPIKE
	or thing.type == MT_WALLSPIKE)
	then
		if (thing.flags & MF_SOLID) and ((thing.type == MT_SPIKE) or (thing.type == MT_WALLSPIKE))
		then
			S_StartSound(plr, thing.info.deathsound)
		end
		for iter in thing.subsector.sector.thinglist()
		do
			if (
				iter.type == thing.type and
				iter.health > 0 and
				( iter.flags & MF_SOLID ) and
				(iter == thing or
				P_AproxDistance(P_AproxDistance(
				thing.x - iter.x, thing.y - iter.y),
				thing.z - iter.z) < 56*thing.scale))
			then
				P_KillMobj(iter, plr, tmthing, 0)
			end
		end
		return 2
		elseif (( thing.flags & MF_MONITOR ))
		then
		if (P_DamageMobj(thing, tmthing, tmthing, 1, 0))
		then
			return 2
		end
	end
end

//break spikes with hyper form
addHook ("MobjMoveCollide", function (plr, thing)
	if plr.player ~= nil and (plr.skin == "sonic")
	or plr.player ~= nil and plr.player.mrce_canhyper == true then
		local dm_threshold = (3*TICRATE)
		if plr and plr.player.powers[pw_super] and plr.player.hypermode == 1 then
			if (thing.type == MT_SPIKE
			or thing.type == MT_WALLSPIKE) then
				if (plr.z + plr.height < thing.z) 
				or (plr.z > thing.z + thing.height) then
					return false
				else
					return Break_shit(plr, thing)
				end
			end
		end
	end
end)

addHook("PlayerCanDamage", function(player, mobj) --hyper forms can pop monitors by just walking into them
	if player.hypermode and player.hypermode == 1
	and player.powers[pw_super] then
		return true
	end
end)

addHook("MobjMoveCollide", function(monitor, plmo)
	if monitor and monitor.valid
	and (monitor.flags & MF_MONITOR)
	and not (plmo.player.ctfteam == 1 and monitor.type == MT_RING_BLUEBOX)
	and not (plmo.player.ctfteam == 2 and monitor.type == MT_RING_REDBOX)
	and (plmo.player)
	and player.hypermode and player.hypermode == 1
    and not (plmo.z > monitor.z+monitor.height+(10*FRACUNIT)) and not (monitor.z > plmo.z+plmo.height+(10*FRACUNIT))
	and monitor.valid and monitor.health then
    local player = plmo.player
    and player.powers[pw_super]
        P_KillMobj(monitor, plmo, plmo)
		return false
	end
end, MT_PLAYER)

//Literally just a port from the source code aside from the slow fall part
addHook("JumpSpinSpecial", function(player)
	if player.hypermode and player.hypermode == 1 and (player.mo.skin == "sonic" or player.mo.skin == "supersonic")
	and player.speed < 5*player.mo.scale
	and P_MobjFlip(player.mo)*player.mo.momz <= 0 then
		if player.speed >= FixedMul(player.runspeed, player.mo.scale) then
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, -3*FRACUNIT)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
	
	if player.hypermode and player.hypermode == 1 and (player.mo.skin == "supersonic" or player.charability == 18)
	and player.speed > 5*player.mo.scale
	and P_MobjFlip(player.mo)*player.mo.momz <= 0 then
		if player.speed >= FixedMul(player.runspeed, player.mo.scale) then
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, 0)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
end)

//Super sparkles

freeslot("SPR_SUSK",
	     "S_SUPERSPARK",
	     "MT_SUPERSPARKLES")

states[S_SUPERSPARK] = {
       sprite = SPR_SUSK,
	   frame = FF_FULLBRIGHT|FF_ANIMATE|A,
	   var1 = 4,
	   var2 = 3
}
mobjinfo[MT_SUPERSPARKLES] = {
         spawnstate = S_SUPERSPARK,
         flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT,
}

local blacklist = {
	["dirk"] = true,
	["altsonic"] = true,
	["pointy"] = true,
	["fluffy"] = true,
	["mcsonic"]= true,
	["sms"] = true
}

local colors = {SKINCOLOR_EMERALD, SKINCOLOR_PURPLE, SKINCOLOR_BLUE, SKINCOLOR_CYAN, SKINCOLOR_ORANGE, SKINCOLOR_RED, SKINCOLOR_GREY}

local function superSpark(mo, amount, fuse, speed1, speed2)
	if not (mo and mo.valid and amount ~= nil and fuse ~= nil and speed1 ~= nil and speed2 ~= nil) then
		return
	end

	for i = 0, amount, 1 do
		local ha = P_RandomRange(0, 360)*ANG1
		local va = P_RandomRange(0, 360)*ANG1
		local speed = P_RandomRange(speed1/FRACUNIT, speed2/FRACUNIT)*FRACUNIT

		local sprk = P_SpawnMobj(mo.x + FixedMul(FixedMul(mo.radius, FixedMul(cos(ha), cos(va))), mo.scale),
								 mo.y + FixedMul(FixedMul(mo.radius, FixedMul(sin(ha), cos(va))), mo.scale),
								 mo.z + FixedMul(FixedMul(mo.radius, sin(va)), mo.scale) + FixedMul(mo.scale, mo.height/2),
								 MT_SUPERSPARKLES)

		sprk.scale = mo.scale
		sprk.fuse = fuse
		sprk.color = colors[P_RandomRange(1, #colors)]

		sprk.momx = FixedMul(FixedMul(speed, FixedMul(cos(ha), cos(va))), mo.scale)
		sprk.momy = FixedMul(FixedMul(speed, FixedMul(sin(ha), cos(va))), mo.scale)
		sprk.momz = FixedMul(FixedMul(speed, sin(va)), mo.scale)
	end
end

sfxinfo[freeslot("sfx_mrfly")].caption = "Flight"

addHook("PlayerSpawn", function(p)
	p.mrce_canhyper = false
	p.mrce_ultrastar = false
	p.mrce_hyperimages = false
	p.skincheck = p.mo.skin
end)

addHook("PlayerThink", function(p)
//buff character stats
	if p.spectator then return false end
	if not p.realmo then return false end
	if p.mo.skin == "sonic" or p.mrce_canhyper == true or p.mrce_ultrastar == true then
		if p.powers[pw_super] and p.hypermode == 1 then
			p.maxdash = skins[p.mo.skin].maxdash * 3 / 2
			p.jumpfactor = skins[p.mo.skin].jumpfactor * 7 / 6
			--p.supercolor = "Silver"  --doesn't actually work, the variable is read only. Maybe a request for 2.2.10?
			p.actionspd = skins[p.mo.skin].actionspd * 5 / 2
			p.mindash = 75 * FRACUNIT
			p.hyperstats = 1
		end
		if p.slowgooptimer == 0 and p.hypermode !=1 then
			p.maxdash = skins[p.mo.skin].maxdash
			p.jumpfactor = skins[p.mo.skin].jumpfactor
			--p.supercolor = "Gold"
			p.actionspd = skins[p.mo.skin].actionspd
			p.mindash = skins[p.mo.skin].mindash
			p.hyperstats = 0
		end
	end
	
//reset hyper values if the player changes skin
	if p.skincheck != p.mo.skin then
		p.mrce_canhyper = false
		p.mrce_ultrastar = false
		p.mrce_hyperimages = false
		p.skincheck = p.mo.skin
	end
	
//give the player rings if they break a invinc monitor while super. Make lemonade out of lemons.
	if p.powers[pw_super] and p.powers[pw_invulnerability] then
		P_GivePlayerRings(p, 20)
		S_StartSound(null, sfx_itemup)
		p.powers[pw_invulnerability] = 0
	end
	
	if p.mrce_ultrastar == true and p.scoreadd < 8 and p.powers[pw_super] and mrce_hyperunlocked == true then
		p.scoreadd = 8
	elseif P_IsObjectOnGround(p.mo) and p.powers[pw_invulnerability] == 0 then
		p.scoreadd = 0
	end
	
//spawn the hyper sparkles
	if p and p.valid and p.mo and p.mo.valid
		and p.mo.skin ~= blacklist
		and p.powers[pw_super]
		and ((p.mrce_canhyper == true and mrce_hyperunlocked == true) or (p.mrce_ultrastar == true and mrce_hyperunlocked == true))
		and not (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS5) then
		superSpark(p.mo, 1, 16, 1, 6*FRACUNIT)
	end

//compatibility checks, allowing custom characters to have hyper forms
	if p.spectator then return false end
	if not p.realmo then return false end
	if p.mo and p.mo.skin == "hms123311" then
		if p.mrce_canhyper != nil then
			p.mrce_canhyper = true
			p.mrce_hyperimages = true
		end
		if mrce_hyperunlocked == true then
			p.pflags = $|PF_GODMODE
		else
			p.pflags = $ & ~PF_GODMODE
		end
	end
	local forwardmove = p.cmd.forwardmove
	if p.mo.skin == "mario" and p.powers[pw_super] and mrce_hyperunlocked == true then
		if p.mariocapeflight != 0 and not P_IsObjectOnGround(p.mo) then
			if forwardmove <= -35 then
				p.mo.momz = -50*P_GetMobjGravity(p.mo)
				if p.startultrafly == true then
					S_StartSound(mo, sfx_mrfly)
					p.startultrafly = false
				end
			else p.startultrafly = true
			end
		end
		if p.mariogroundpound and  P_IsObjectOnGround(p.mo) and p.startpoundnuke == true then
			P_NukeEnemies(p.mo, p.mo, 600*FRACUNIT)
			p.startpoundnuke = false
			if p.screenflash == true then
				P_FlashPal(p, PAL_WHITE, 7)
			end
		elseif not P_IsObjectOnGround(p.mo) then
			p.startpoundnuke = true
		end
	end
	if p.mo and p.mo.skin == "pointy" then
		p.mrce_hyperimages = false
		p.mrce_canhyper = true
		p.mrce_ultrastar = false
	end
	if p.mo .skin == "tails"
	or p.mo.skin == "bandages"
	or p.mo.skin == "fang"
	or p.mo.skin == "metalsonic"
	or p.mo.skin == "amy"
	or p.mo.skin == "knuckles"
	or p.mo.skin == "mcsonic"
	or p.mo.skin == "crystalsonic"
	or p.mo.skin == "altsonic"
	or p.mo.skin == "eggman"
	or p.mo.skin == "jana" then
		p.mrce_hyperimages = false
		p.mrce_canhyper = false
		p.mrce_ultrastar = false
	end
	if p.mo.skin == "modernsonic"
	or p.mo.skin == "HMS123311"
	or p.mo.skin == "dirk"
	or p.mo.skin == "yoshi"
	or p.mo.skin == "sonic"
	or p.mo.skin == "supersonic" then
		p.mrce_hyperimages = true
		p.mrce_canhyper = true
		p.mrce_ultrastar = false
	end
	if p.mo.skin == "metalsonic" then
		p.mrce_hyperimages = true
		p.mrce_ultrastar = false
	end
	if p.mo.skin == "mario" then
		p.mrce_ultrastar = true
		p.mrce_hyperimages = false
		p.mrce_canhyper = false
	end
end)

--Triggers an event if you're SA-Sonic.

local function YuHyperMemories(line, so)
	if so and so.valid and so.player and so.player.yusonictable and not so.player.yusonictable.hyperpower and so.skin == "adventuresonic" and so.health then
		S_StartSound(so, sfx_s3k7d)
		S_StartSound(so, sfx_s3kcel)
		so.player.yusonictable.hypermemories = 220
		so.player.yusonictable.superflash = 9*FRACUNIT
		so.player.yusonictable.hyperpower = true
		return false
	end
end
addHook("LinedefExecute", YuHyperMemories, "YUMEMO")