//Bomb Throw Ability - By Simon_T
//This is free to be used in other people's mods
//But it would be great if you credit me as the original creator ;)

addHook("ThinkFrame", do
	for player in players.iterate do
		if player.mo and player.mo.skin == "fang" then //If you're Fang
			if not player.exiting then //If you're leaving a level, you won't need them, right?
				if not (P_PlayerInPain(player) or (player.playerstate == PST_DEAD) or P_IsObjectOnGround(player.mo)) then //Hurt/dead weasels can't throw bombs
					if ((player.cmd.buttons & BT_SPIN) and not (player.weapondelay)) then //If you press Toss Flag...
						S_StartSound(player.mo, sfx_s3k51) //...Play the bomb throwing sound...
						local bomb = P_SpawnMobj(player.mo.x, player.mo.y, player.mo.z + player.mo.height, MT_FBOMB) //...And spawn the bomb itself
						bomb.target = player.mo //The bomb can't hurt its user
						if (player.mo.momz > 0) then //If you're jumping/springing/bouncing...
							bomb.momz = (player.mo.momz)+(9*FRACUNIT) //...The bomb gets some height boost
						else //But if you're falling...
							bomb.momz = 7*FRACUNIT //...The bomb just flies at this fixed height
						end
						P_InstaThrust(bomb, player.mo.angle, ((player.speed)+(8*FRACUNIT))) //The bomb describes an arc while flying, affected by your current speed
						if (player.powers[pw_super]) then //If you're Super Fang...
							player.weapondelay = 23 //...You can throw bombs 3X faster
						else //But if that's not the case...
							player.weapondelay = TICRATE*2 //...You have to wait two seconds between bombs
						end
					end
				end
			end
		end
	end
end)