--This addon is marked as reusable, so do what you want with it. ~Krabs

--Which skin gets the rebound dash?
local rbdskin = "sonic"

--Dash
local DASHSPD = 48 * FRACUNIT
local DASHLENGTH = 24
local DASHZPERCENT = 50

--Factors
local WATERFACTOR = FRACUNIT * 2/3
local SUPERFACTOR = FRACUNIT * 7/4

--Wall bounce vertical
local WALLBOUNCEZ = 11 * FRACUNIT
local WEAKWALLBOUNCEZ = 4 * FRACUNIT
local NOCLWALLBOUNCEZ = 3 * FRACUNIT
local SECONDBOUNCEZMULT = 118

--Wall bounce thrust in wall direction
local WALLTHRUST = 5 * FRACUNIT
local WEAKWALLTHRUST = FRACUNIT / 5
local NOCLIMBWALLTHRUST = 3 * FRACUNIT

--Wall bounce horizontal
local BOUNCEPERCENT = 17
local WEAKBOUNCEPERCENT = 3

--Badnik bounce
local BADNIKZMULT = 120
local BADNIKSPEED = 72
local BADNIKBUMP = 20

--Assign sprites, objects, and sfx
freeslot(
	"SPR_REBO",
	"MT_REBOUND",
	"S_REBOUND",
	"sfx_airdsh",
	"sfx_bounc1",
	"sfx_bounc2",
	"sfx_strdsh"
)

--Closed captions
sfxinfo[sfx_airdsh] = {
	singular = false,
	caption = "Dash"
}
sfxinfo[sfx_strdsh] = {
	singular = false,
	caption = "Strong Dash"
}
sfxinfo[sfx_bounc1] = {
	singular = false,
	caption = "Rebound"
}
sfxinfo[sfx_bounc2] = {
	singular = false,
	caption = "Heavy Rebound"
}

--Wallbounce ring effect object
mobjinfo[MT_REBOUND] = {
	doomednum = -1,
	spawnstate = S_REBOUND,
	flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY
}
states[S_REBOUND] = {
	sprite = SPR_REBO,
	frame = TR_TRANS50|FF_PAPERSPRITE|A
}

--FixedLerp
local function FixedLerp(a, b, w)
    return FixedMul((FRACUNIT - w), a) + FixedMul(w, b)
end

--Functions needed to do the bounce calculations (these could also be used to allow bounces off of sloped terrain, but I don't think we need to do that for this ability)
local function SphereToCartesian(alpha, beta)
	local t = {}

	t.x = FixedMul(cos(alpha), cos(beta))
	t.y = FixedMul(sin(alpha), cos(beta))
	t.z = sin(beta)

	return t
end

local function FixedDotProduct3D(a, b)
	return FixedMul(a.x, b.x) + FixedMul(a.y, b.y) + FixedMul(a.z, b.z)
end

local function FixedScalar3D(vect, scalar)
	local vect2 = {}
	vect2.x = FixedMul(vect.x, scalar)
	vect2.y = FixedMul(vect.y, scalar)
	--vect2.z = FixedMul(vect.z, scalar)
	vect2.z = vect.z
	
	return vect2
end

local function Percent3D(vect, percent)
	local vect2 = {}
	vect2.x = vect.x * percent / 100
	vect2.y = vect.y * percent / 100
	--vect2.z = FixedMul(vect.z, scalar)
	vect2.z = vect.z
	
	return vect2
end

local function VectAdd3D(vecta, vectb)
	local vsum = {}
	vsum.x = vecta.x + vectb.x
	vsum.y = vecta.y + vectb.y
	vsum.z = vecta.z + vectb.z
	
	return vsum
end

local function VectorBounce(mo,vmom,vslopenorm,percent)
	local player = mo.player
	if (player == nil) or (player.playerstate != PST_LIVE) then return end
	
	local vbounce = Percent3D(VectAdd3D(FixedScalar3D(FixedScalar3D(vslopenorm, FixedDotProduct3D(vmom,vslopenorm)), -2 * FRACUNIT), vmom), percent)
	
	mo.momx = vbounce.x
	mo.momy = vbounce.y
	mo.momz = vbounce.z
end


--Rebound dash
addHook("AbilitySpecial", function(player)
	local mo = player.mo
	if not (player.charability == 18) then return false end
	if (player.pflags & (PF_THOKKED|PF_SHIELDABILITY)) and player.doubledash != true then return true end
	
	if player.doubledash == true then
		player.doubledash = false
	end
	
	--Flags, timers, state
	mo.state = S_PLAY_JUMP
	player.pflags = $1 | PF_JUMPED
	player.pflags = $1 | PF_THOKKED
	player.pflags = $1 & ~(PF_SPINNING|PF_STARTDASH)
	player.dashticsleft = DASHLENGTH * 10
	player.holdingjump = true
	
	--Screenshake
	if player == consoleplayer and player.powers[pw_super] then
		local shake = 10
		local shaketics = 3
		P_StartQuake(shake * FRACUNIT, shaketics)
	end
	
	--XY Momentum, timer
	local dashsp = DASHSPD
	if mo.eflags & MFE_UNDERWATER then
		dashsp = FixedMul($, WATERFACTOR)
	end
	if player.powers[pw_super] then
		dashsp = FixedMul($, SUPERFACTOR)
	end
	
	local momangle = R_PointToAngle2(0,0,player.rmomx,player.rmomy)
	local anglediff = AngleFixed(abs(momangle - mo.angle)) / FRACUNIT
	local pspd = player.speed
	
	local speedpercentage = max(0, min(100, (100 - (anglediff - 50))))
	pspd = $ * speedpercentage / 100
	
	P_InstaThrust(mo, mo.angle, FixedMul(max(dashsp,pspd), mo.scale))
	
	//Z momentum
	/*if mo.eflags & MFE_VERTICALFLIP
		mo.momz = min($,0)
	else
		mo.momz = max($,0)
	end*/
	if mo.eflags & MFE_VERTICALFLIP then
		if mo.momz > 0 then
			mo.momz = $ * DASHZPERCENT / 100
		end
	else
		if mo.momz < 0 then
			mo.momz = $ * DASHZPERCENT / 100
		end
	end
	
	--Visual and sfx
	player.lockangle = mo.angle
	if player.doubledash != nil and player.hyperbonus == 0 then
		S_StartSound(mo,sfx_strdsh)
	else
		S_StartSound(mo,sfx_airdsh)
	end
	local circle = P_SpawnMobjFromMobj(mo, 0, 0, mo.scale * 24, MT_REBOUND)
	circle.angle = mo.angle + ANGLE_90
	circle.fuse = 7
	circle.scale = FRACUNIT / 3
	circle.destscale = 10*FRACUNIT
	circle.colorized = true
	circle.color = mo.color
	circle.momx = -mo.momx / 2
	circle.momy = -mo.momy / 2
	
	return true
end)

addHook("ThinkFrame", function()
	for player in players.iterate() do
		if player.spectator then return false end
		if not player.realmo then return false end
		local mo = player.mo
		if not (player.charability == 18) then continue end
		if (player == nil) or (player.playerstate != PST_LIVE) then continue end
		
		if player.prevpflags != nil then
			--Roll after dash
			if P_IsObjectOnGround(mo) and (player.prevpflags & PF_THOKKED) and (player.cmd.buttons & BT_USE)
			and (FixedHypot(mo.momx, mo.momy) > (5 * mo.scale)) then
				mo.state = S_PLAY_ROLL
				player.pflags = $ | PF_SPINNING
				S_StartSound(mo,sfx_spin)
			end
		end
		
		--Reset dash counter and timer
		if P_IsObjectOnGround(mo) or not (player.pflags & PF_THOKKED) then
			player.dashticsleft = nil
			player.holdingjump = nil
		end
		
		if P_IsObjectOnGround(mo) then
			player.doubledash = nil
			if player.hypermode == 1 then
				player.hyperbonus = 2 --handles resetting hyper bonus, which allows to rebound 3 times instead of 2 when hyper. setting this to 1 will remove that buff,
			else							--while increasing it will give you more bonus rebounds. setting it to 0 will make you only able to rebound a single time, so don't do that
				player.hyperbonus = 0
			end
		end
		player.prevpflags = player.pflags
	end
end)

addHook("PostThinkFrame", function()
	for player in players.iterate() do
		if player.spectator then return false end
		if not player.realmo then return false end
		local mo = player.mo
		if not (player.charability == 18) then continue end
	
	if (player == nil) or (player.playerstate != PST_LIVE) then continue end
	
		if (mo.previousx == nil) or (mo.previousy == nil) or (mo.previousz == nil) then
			mo.previousx = mo.x
			mo.previousy = mo.y
			mo.previousz = mo.z
		end
		
		/*if (player.speed > 15*FRACUNIT) and (player.pflags & PF_SPINNING) and not (player.pflags & PF_THOKKED)
            for i = 0, 9
                local percent = FRACUNIT * (i * 10) / 100
                local trail = P_SpawnGhostMobj(mo)
                local tx = FixedLerp(mo.x,mo.previousx,percent)
                local ty = FixedLerp(mo.y,mo.previousy,percent)
                local tz = FixedLerp(mo.z + 3*FRACUNIT,mo.previousz + 4*FRACUNIT,percent)
                P_TeleportMove(trail, tx, ty, tz - 7 * FRACUNIT)
                trail.fuse = 13
                trail.state = S_THOK
                trail.scalespeed = FRACUNIT/12
                trail.scale = (FRACUNIT * 5/6) - (i * FRACUNIT/120)
                trail.destscale = 0
                trail.frame = TR_TRANS70|A
            end
		end*/

		if player.dashticsleft then
			player.drawangle = player.lockangle
			
			if mo.state != S_PLAY_JUMP then
				player.doubledash = nil
				player.dashticsleft = nil
			
			else
				--Check if the player is still holding jump
				if player.holdingjump then
					player.holdingjump = (player.cmd.buttons & BT_JUMP)
					
					--Short dash by tapping the button and not holding forward
					if not player.holdingjump and player.cmd.forwardmove <= 10 then
						if player.dashticsleft > 10*DASHLENGTH - 30 then
							mo.momx = FixedDiv($, 2 * FRACUNIT)
							mo.momy = FixedDiv($, 2 * FRACUNIT)
						elseif player.dashticsleft > 10*DASHLENGTH - 50 then
							mo.momx = FixedDiv(FixedMul($, 2 * FRACUNIT), 3 * FRACUNIT)
							mo.momy = FixedDiv(FixedMul($, 2 * FRACUNIT), 3 * FRACUNIT)
						elseif player.dashticsleft > 10*DASHLENGTH - 60 then
							mo.momx = FixedDiv(FixedMul($, 3 * FRACUNIT), 4 * FRACUNIT)
							mo.momy = FixedDiv(FixedMul($, 3 * FRACUNIT), 4 * FRACUNIT)
						elseif player.dashticsleft > 10*DASHLENGTH - 70 then
							mo.momx = FixedDiv(FixedMul($, 4 * FRACUNIT), 5 * FRACUNIT)
							mo.momy = FixedDiv(FixedMul($, 4 * FRACUNIT), 5 * FRACUNIT)
						end
					end
				end
				if player.holdingjump then
					--Thok trail (which disappears a bit earlier in order to make sure the player *really* knows they messed up the timing when they hit a wall too late)
					if player.dashticsleft > 80 then
						for i = 0, 9 do
							local percent = FRACUNIT * (i * 10) / 100
							local trail = P_SpawnGhostMobj(mo)
							local tx = FixedLerp(mo.x,mo.previousx,percent)
							local ty = FixedLerp(mo.y,mo.previousy,percent)
							local tz = FixedLerp(mo.z + 3*FRACUNIT,mo.previousz + 4*FRACUNIT,percent)
							P_TeleportMove(trail, tx, ty, tz - 7 * FRACUNIT)
							trail.fuse = 13
							trail.state = S_THOK
							trail.scalespeed = FRACUNIT/12
							trail.scale = (FRACUNIT * 4/5) - (i * FRACUNIT/120)
							trail.destscale = 0
							trail.frame = TR_TRANS80|A
							if player.mo.scale != 1*FRACUNIT then
								trail.scale = FixedMul((FRACUNIT * 4/5) - (i * FRACUNIT/120), player.mo.scale)
							end
							if player.glowaura == true then
								trail.blendmode = AST_ADD
							end
						end
					end
					player.dashticsleft = $ - 10
				else
					--Lesser thok trail (player let go of the jump button, so they can't wallbounce anymore)
					if player.dashticsleft > 80 then
						for i = 0, 9 do
							local percent = FRACUNIT * (i * 10) / 100
							local trail = P_SpawnGhostMobj(mo)
							local tx = FixedLerp(mo.x,mo.previousx,percent)
							local ty = FixedLerp(mo.y,mo.previousy,percent)
							local tz = FixedLerp(mo.z + 3*FRACUNIT,mo.previousz + 4*FRACUNIT,percent)
							P_TeleportMove(trail, tx, ty, tz - 3 * FRACUNIT)
							trail.fuse = 9
							trail.state = S_THOK
							trail.scalespeed = FRACUNIT/15
							trail.scale = (FRACUNIT * 3/5) - (i * FRACUNIT/150)
							trail.destscale = 0
							trail.frame = TR_TRANS90|A
							if player.mo.scale != 1*FRACUNIT then
								trail.scale = FixedMul((FRACUNIT * 3/5) - (i * FRACUNIT/150), player.mo.scale)
							end
							if player.glowaura == true then
								trail.blendmode = AST_ADD
							end
						end
					end
					player.dashticsleft = $ - 15
				end
			end
		end
		
		--Reset bounceline
		player.bounceline = nil
		mo.previousx = mo.x
		mo.previousy = mo.y
		mo.previousz = mo.z
	end
end)

addHook("MobjLineCollide", function(mo, line)
	if not mo or not mo.valid then return end
	local player = mo.player
	if not (player.charability == 18) then return end
	if (player == nil) or (player.playerstate != PST_LIVE) then return end
	if (not player.dashticsleft) or (player.dashticsleft <= 0) then return end
	
	--Horizon line
	if line.special == 41 then return end
	
	--Set the bounceline so we can bounce off it later in case that line actually blocked the player from moving
	local side = P_PointOnLineSide(mo.x,mo.y,line)
	local sector = nil
	
	--One-sided walls
	if line.backsector == nil then
		player.bounceline = line
		player.bounceside = 0
		return
	end
	if line.frontsector == nil then
		player.bounceline = line
		player.bounceside = 1
		return
	end
	
	--Which side are we hitting the wall from?
	if side == 1 then
		sector = line.frontsector
	else
		sector = line.backsector
	end
	
	--Impassible line
	if (line.flags & ML_IMPASSIBLE) and line.frontside.midtexture then
		player.bounceline = line
		player.bounceside = side
		return
	end
	
	if sector == nil then return end
	
	--Polyobject
	for i = 0, #sector.lines do
		local li = sector.lines[i]
		if li == nil then continue end
		
		if li.special == 20 then--First line of polyobject
			local topheight = sector.ceilingheight
			local bottomheight = sector.floorheight
			
			if (topheight < mo.z) then
				return
			end

			if (bottomheight > mo.z + mo.height) then
				return
			end

			player.bounceline = line
			player.bounceside = side
			return
		end
	end
	
	--Standard
	local ceilz = sector.ceilingheight
	if sector.c_slope then
		ceilz = P_GetZAt(sector.c_slope, mo.x, mo.y)
	end
	if (ceilz < mo.z + mo.height) then
		if sector.ceilingpic != "F_SKY1" then
			player.bounceline = line
			player.bounceside = side
		end
		return
	end
	local floorz = sector.floorheight
	if sector.f_slope then
		floorz = P_GetZAt(sector.f_slope, mo.x, mo.y)
	end
	if (floorz > mo.z) then
		player.bounceline = line
		player.bounceside = side
		return
	end
	
	--FOF
	for rover in sector.ffloors() do
		if not (rover.flags & FF_EXISTS) or not (rover.flags & FF_BLOCKPLAYER) then
			continue
		end

		local topheight = rover.topheight
		local bottomheight = rover.bottomheight

		if (rover.t_slope) then
			topheight = P_GetZAt(rover.t_slope, mo.x, mo.y)
		end
		if (rover.b_slope) then
			bottomheight = P_GetZAt(rover.b_slope, mo.x, mo.y)
		end

		if (topheight < mo.z) then
			continue
		end

		if (bottomheight > mo.z + mo.height) then
			continue
		end
	
		player.bounceline = line
		player.bounceside = side
		if (rover.flags & FF_BUSTUP) and not (rover.flags & FF_STRONGBUST) then
			player.bustsector = sector
			player.bustrover = rover
		end
		break
	end
	
	--Solid midtexture
	--TODO: once textures[] is exposed to lua, change this so it properly checks the height of a solid midtexture and decides if it's blocking the player or not.
	if (line.flags & ML_EFFECT4) and line.frontside.midtexture then
		player.bounceline = line
		player.bounceside = side
		return
	end
end, MT_PLAYER)

addHook("PreThinkFrame", function()	
	for player in players.iterate() do
		local mo = player.mo
		if not (player.charability == 18) then return end
		if (player == nil) or (player.playerstate != PST_LIVE) then return end
	end
end)

local function DoWallBounce(mo,player,wallnormangle,walltype,side,reflect)
	--Wall type
	local bouncy = (walltype == 2)
	local hbouncy = (walltype == 3)
	local vbouncy = (walltype == 4)
	local nocl = (walltype == 1)

	--Hold jump button for better bounce, don't hold for small bounce
	local bigbounce = player.holdingjump
	
	--Calculate angle
	local vwallnorm = SphereToCartesian(wallnormangle, 0)
	
	--Noclimb wall
	if nocl then
		bigbounce = false
		S_StartSound(mo,sfx_s3k9e)
	end
	
	--Bounce stats based on the situation
	local percent = BOUNCEPERCENT
	if not bigbounce then
		percent = WEAKBOUNCEPERCENT
	end
	local bouncez = WEAKWALLBOUNCEZ
	
	--Hold jump button for better bounce, don't hold for small bounce
	if bigbounce and not nocl then
		bouncez = WALLBOUNCEZ
	elseif nocl then
		bouncez = NOCLWALLBOUNCEZ
	end
	
	--Screenshake
	if player == consoleplayer and (bigbounce or nocl) then
		local shake = 12
		local shaketics = 3
		if player.powers[pw_super] then
			shake = 18
			shaketics = 5
		end
		P_StartQuake(shake * FRACUNIT, shaketics)
	end
	
	--mobj momentum vector
	local vmom = {}
	vmom.x = mo.momx
	vmom.y = mo.momy
	vmom.z = mo.momz
	
	--Reset jump flags
	player.pflags = $ | PF_JUMPED
	player.pflags = $ | PF_STARTJUMP
	
	local weak = false
	if not bigbounce and not nocl then
		--Weak bump sfx
		S_StartSoundAtVolume(mo,sfx_s3k5d,155)
		weak = true
	end
	
	--Double dash is allowed after a wall jump
	if player.doubledash == nil or player.hyperbonus > 0 then
		player.doubledash = true
		if player.hypermode == 1 then
			player.hyperbonus = $ - 1
			player.pflags = $1 & ~PF_THOKKED
		end
		--1st bounce sfx
		if not weak then
			S_StartSoundAtVolume(mo,sfx_bounc1,175)
		end
	elseif player.hyperbonus == 0 then
		player.doubledash = false
		mo.state = S_PLAY_SPRING
		bouncez = $ * SECONDBOUNCEZMULT / 100
		percent = $ + 12
		player.pflags = $ | PF_NOJUMPDAMAGE
		--2nd bounce sfx
		if not weak then
			S_StartSound(mo,sfx_bounc2)
		end
	end
	
	--Wallthrust (the strength of momentum applied in the direction of the wall norm)
	local wallth = WALLTHRUST
	if not bigbounce then
		wallth = WEAKWALLTHRUST
	end
	if nocl then
		wallth = NOCLIMBWALLTHRUST
	end
	
	--Wall type
	if bouncy then
		S_StartSound(mo,sfx_s3k87)
		bouncez = $ + 6*FRACUNIT
		percent = $ + 6
	end
	if hbouncy then
		wallth = $ + 40*FRACUNIT
		S_StartSound(mo,sfx_cdfm74)
	end
	if vbouncy then
		wallth = $ / 2
		percent = $ / 4
		bouncez = $ + 11*FRACUNIT
		S_StartSound(mo,sfx_cdfm62)
	end
	
	if player.mo.scale != 1*FRACUNIT then
		wallth = FixedMul($, player.mo.scale)
	end
	
	--Do the horizontal bounce
	if reflect == 1 then--wall
		VectorBounce(mo,vmom,vwallnorm,percent)
		if side == 1 then
			P_Thrust(mo, wallnormangle, wallth)
			player.drawangle = wallnormangle
		else
			P_Thrust(mo, wallnormangle, -wallth)
			player.drawangle = wallnormangle + ANGLE_180
		end
	elseif reflect == 0 then--boss
		percent = $ - 10
		mo.momz = $ + 2 * FRACUNIT
		mo.momx = $ * percent / 100
		mo.momy = $ * percent / 100
		P_Thrust(mo, wallnormangle, wallth)
		player.drawangle = wallnormangle
	elseif reflect == 2 then--badnik/monitor
		local zm = max(0, (player.dashticsleft * 6/10) + 38)//The badnik bounce force reduces based on the dash length
		bouncez = max(abs(mo.momz * (zm) / 100), $) * BADNIKZMULT / 100
		if bigbounce then
			mo.momx = $ * BADNIKSPEED / 100
			mo.momy = $ * BADNIKSPEED / 100
		else
			mo.momx = $ * BADNIKBUMP / 100
			mo.momy = $ * BADNIKBUMP / 100
		end
	elseif reflect == 3 then--egg colosseum
		mo.momz = $ * 3 / 4
		player.drawangle = wallnormangle
	end
	
	--Reset stuff
	player.bounceline = nil
	player.dashticsleft = nil
	
	--Vertical boost
	if mo.eflags & MFE_UNDERWATER then
		bouncez = FixedMul($, WATERFACTOR)
	end
	if player.powers[pw_super] then
		bouncez = FixedMul($, SUPERFACTOR)
	end
	if player.mo.scale != 1*FRACUNIT then
		bouncez = FixedMul($, player.mo.scale)
	end
	if mo.eflags & MFE_VERTICALFLIP then
		mo.momz = min($,-bouncez)
	else
		mo.momz = max($,bouncez)
	end
	
	--Wallbounce dust
	if nocl then
		local dustcount = 6
		while dustcount > 0 do
			dustcount = $ - 1
			local dust = P_SpawnMobjFromMobj(mo, 0, 0, mo.scale * 18, MT_SPINDUST)
			dust.state = S_MINECARTSPARK
			dust.momx = (mo.momx * 3) + (P_RandomRange(-20,20) * FRACUNIT)
			dust.momy = (mo.momy * 3) + (P_RandomRange(-20,20) * FRACUNIT)
			dust.momz = P_RandomRange(-3,3) * FRACUNIT
			dust.scale = FRACUNIT * P_RandomRange(50,125) / 100
			dust.destscale = 0
			dust.angle = P_RandomRange(0,ANGLE_180)
			dust.fuse = 10
		end
	else
		local dustcount = 10
		if not bigbounce then
			dustcount = 5
		end
		while dustcount > 0 do
			dustcount = $ - 1
			local dust = P_SpawnMobjFromMobj(mo, 0, 0, mo.scale * 18, MT_SPINDUST)
			dust.momx = (mo.momx * 2) + (P_RandomRange(-10,10) * FRACUNIT)
			dust.momy = (mo.momy * 2) + (P_RandomRange(-10,10) * FRACUNIT)
			dust.momz = P_RandomRange(-3,3) * FRACUNIT
			dust.scale = FRACUNIT * P_RandomRange(50,125) / 100
			dust.destscale = 0
			dust.color = mo.color
			dust.colorized = true
		end
	end
	
	if bigbounce then
		--Wallbounce big puff of smoke
		local boom = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_SPINDUST)
		boom.state = S_XPLD3
		boom.colorized = true
		boom.color = mo.color
		if player.mo.scale != 1*FRACUNIT then
			boom.scale = FixedMul($, player.mo.scale)
		end
	end
	
	--Wallbounce circle effect
	local circle = P_SpawnMobjFromMobj(mo, 0, 0, mo.scale * 19, MT_REBOUND)
	circle.angle = wallnormangle + ANGLE_90
	circle.fuse = 9
	circle.scale = 0
	circle.destscale = 10*FRACUNIT
	circle.color = mo.color
	circle.colorized = true
	
	if not bigbounce then
		circle.fuse = 6
	end
	if player.mo.scale != 1*FRACUNIT then
		circle.scale = FixedMul($, player.mo.scale)
	end
end

addHook("MobjDeath", function(enemy, mo)
	if not mo or not mo.valid then return end
	local player = mo.player
	if (player == nil) or (player.playerstate != PST_LIVE) then return end
	if not (player.charability == 18) then return end
	if (not player.dashticsleft) or (player.dashticsleft <= 0) then return end
	
	if enemy and enemy.valid and (enemy.flags & MF_MONITOR) then
		local wallnormangle = R_PointToAngle2(0, 0, mo.momx, mo.momy)
		DoWallBounce(mo,player,wallnormangle,false,0,2)
	end
end)

addHook("MobjDamage", function(enemy, mo)
	if not mo or not mo.valid then return end
	local player = mo.player
	if (player == nil) or (player.playerstate != PST_LIVE) then return end
	if not (player.charability == 18) then return end
	if (not player.dashticsleft) or (player.dashticsleft <= 0) then return end
	
	if enemy and enemy.valid and (enemy.flags & MF_ENEMY or enemy.flags & MF_BOSS) then
		local wallnormangle = R_PointToAngle2(0, 0, mo.momx, mo.momy)
		if enemy.type == MT_EGGMOBILE4 then
			DoWallBounce(mo,player,wallnormangle,false,0,3)
		elseif (enemy.info.spawnhealth > 1) then
			DoWallBounce(mo,player,wallnormangle,false,0,0)
		else
			DoWallBounce(mo,player,wallnormangle,false,0,2)
		end
	end
end)

addHook("MobjMoveCollide", function(mo, other)
	if not mo or not mo.valid then return end
	local player = mo.player
	if not (player.charability == 18) then return end
	if (player == nil) or (player.playerstate != PST_LIVE) then return end
	if (not player.dashticsleft) or (player.dashticsleft <= 0) then return end
	if (other.flags & MF_SOLID) and not (other.flags & MF_ENEMY or other.flags & MF_BOSS or other.flags & MF_MONITOR or other.flags & MF_PAIN) and not (other.type == MT_PLAYER) then
		if (mo.z < other.z + other.height) and (mo.z + mo.height >= other.z) then
			if (other.flags & MF_PUSHABLE) then
				other.momx = $ + (mo.momx / 2)
				other.momy = $ + (mo.momy / 2)
				other.momz = 3 * FRACUNIT
				local wallnormangle = R_PointToAngle2(0, 0, mo.momx, mo.momy)
				DoWallBounce(mo,player,wallnormangle,false,0,1)
				return true
			end
			local wallnormangle = R_PointToAngle2(0, 0, mo.momx, mo.momy)
			DoWallBounce(mo,player,wallnormangle,false,0,1)
			return false
		end
	end
end, MT_PLAYER)

addHook("MobjMoveBlocked", function(mo)
	local player = mo.player
	if (not player.dashticsleft) or (player.dashticsleft <= 0) then return end
	local line = player.bounceline
	if line == nil then return end

	--Noclimb walls result in a super weak bounce
	local nocl = player.bounceline.flags & ML_NOCLIMB
	
	--Bouncy walls result in a beeg bounce
	local bouncy = (player.bounceline.special == 999)
	local hbouncy = (player.bounceline.special == 998)
	local vbouncy = (player.bounceline.special == 997)
	
	--Bustable FOFs are busted
	if player.bustsector and player.bustrover and player.holdingjump then
		EV_CrumbleChain(nil, player.bustrover)
		player.bustsector = nil
		player.bustrover = nil
	end
	
	local wallnormangle = R_PointToAngle2(line.v1.x, line.v1.y, line.v2.x, line.v2.y) + ANGLE_90
	
	if bouncy then
		DoWallBounce(mo,player,wallnormangle,2,player.bounceside,1)
	elseif hbouncy then
		DoWallBounce(mo,player,wallnormangle,3,player.bounceside,1)
	elseif vbouncy then
		DoWallBounce(mo,player,wallnormangle,4,player.bounceside,1)
	else
		if nocl then
			nocl = 1
		end
		DoWallBounce(mo,player,wallnormangle,nocl,player.bounceside,1)
	end
end, MT_PLAYER)

addHook("MobjThinker", function(mobj)
	if mobj.interpmode != nil then -- interpmode is never nil on uncapped
        mobj.interpmode = 2 -- set interpmode 1 for smooth no interp
	end
end, MT_REBOUND)

--FixedLerp
local function FixedLerp(a, b, w)
    return FixedMul((FRACUNIT - w), a) + FixedMul(w, b)
end

addHook("ThinkFrame", function()
	for p in players.iterate
		if p.spectator then return false end
		if not p.realmo then return false end
	
		if p.mo.skin == "sms" or p.mo.skin == "mario" then
			return 
		end
		
		if p.glowaura != true then
			return
		end

		if (p.charability2 == CA2_SPINDASH or p.mo.state == S_PLAY_ROLL or p.mo.state == S_PLAY_SPINDASH) and p.spinitem then
			p.spinitem = 0
		end
		if (p.charability == CA_HOMINGTHOK) and p.thokitem then
			p.thokitem = 0
		end
	end
end)

addHook("PostThinkFrame", function()
	for player in players.iterate() do
		local mo = player.mo
		if not (mo and mo.valid) or (mo.skin == "inazuma") or (mo.skin == "sms")  or (mo.skin == "mario") or (player.glowaura != true) then continue end
		if (player == nil) or (player.playerstate != PST_LIVE) then continue end
			
		if (mo.previousx == nil) or (mo.previousy == nil) or (mo.previousz == nil) then
			mo.previousx = mo.x
			mo.previousy = mo.y
			mo.previousz = mo.z
		end
		
		if (player.speed > 15*FRACUNIT) and (player.pflags & PF_SPINNING)
		or player.homing  then
			for i = 0, 9 do
				local percent = FRACUNIT * (i * 10) / 100
				local trail = P_SpawnMobj(mo.x, mo.y, mo.z, MT_THOK)
				local tx = FixedLerp(mo.x,mo.previousx,percent)
				local ty = FixedLerp(mo.y,mo.previousy,percent)
				local tz = FixedLerp(mo.z + 3*FRACUNIT,mo.previousz + 4*FRACUNIT,percent)
				P_TeleportMove(trail, tx, ty, tz - 7 * FRACUNIT)
				if (player.mo.skin == "shadow") and player.mo.color == SKINCOLOR_BLACK then
					trail.color = SKINCOLOR_YELLOW
				elseif (player.mo.skin == "metalsonic") and player.mo.color == SKINCOLOR_COBALT then
					trail.color = SKINCOLOR_PURPLE
				else
					trail.color = mo.color
				end
				trail.fuse = 13
				trail.state = S_THOK
				trail.scalespeed = FRACUNIT/12
				trail.scale = (FRACUNIT * 5/6) - (i * FRACUNIT/120)
				trail.destscale = 0
				trail.frame = TR_TRANS70|A
			end
		end

		mo.previousx = mo.x
		mo.previousy = mo.y
		mo.previousz = mo.z
	end
end)

//Begin thokaura
freeslot("MT_FAKETHOK", "MT_FAKESPIN")
freeslot("S_FAKETHOK", "S_FAKESPIN")
freeslot("SPR_FTHK", "SPR_FSPN")
 
local thok = MT_FAKETHOK
mobjinfo[thok].spawnstate = S_FAKETHOK
mobjinfo[thok].health = 1000
mobjinfo[thok].radius = 20*FRACUNIT
mobjinfo[thok].height = 41*FRACUNIT
mobjinfo[thok].speed = 8
mobjinfo[thok].flags = MF_SCENERY|MF_NOGRAVITY|MF_NOCLIP
mobjinfo[thok].dispoffset = 1
 
local spin = MT_FAKESPIN
mobjinfo[spin].spawnstate = S_FAKESPIN
mobjinfo[spin].health = 1000
mobjinfo[spin].radius = 20*FRACUNIT
mobjinfo[spin].height = 41*FRACUNIT
mobjinfo[spin].speed = 8
mobjinfo[spin].flags = MF_SCENERY|MF_NOGRAVITY|MF_NOCLIP
mobjinfo[spin].dispoffset = 1
 
states[S_FAKETHOK].sprite = SPR_FTHK
states[S_FAKETHOK].frame = A|FF_TRANS60
states[S_FAKETHOK].tics = 1
states[S_FAKETHOK].nextstate = S_NULL
 
states[S_FAKESPIN].sprite = SPR_FSPN
states[S_FAKESPIN].frame = A|FF_TRANS60
states[S_FAKESPIN].tics = 1
states[S_FAKESPIN].nextstate = S_NULL
 
addHook("ThinkFrame", function(p)
	for p in players.iterate
		if p.glowaura == true then
			if (p.mo.skin == "sonic") or (p.mo.skin == "metalsonic") or (p.mo.skin == "shadow") then
				if (p.mo.state == S_PLAY_JUMP) then
					local fakethok = P_SpawnMobjFromMobj(p.mo, 0*FRACUNIT, 0*FRACUNIT, -3*FRACUNIT, MT_FAKETHOK)
						if (p.mo.skin == "shadow") and (p.mo.color == SKINCOLOR_BLACK) then
							fakethok.color = SKINCOLOR_YELLOW
						elseif (p.mo.skin == "metalsonic") and (p.mo.color == SKINCOLOR_COBALT) then
							fakethok.color = SKINCOLOR_PURPLE
						elseif (p.mo.skin == "sonic") and (p.powers[pw_super] and mrce_hyperunlocked == true) then
							fakethok.color = SKINCOLOR_WHITE
						else
							fakethok.color = p.mo.color
						end
					fakethok.blendmode = AST_ADD
					if (p.mo.flags2 & MF2_OBJECTFLIP) and (p.mo.eflags & MFE_VERTICALFLIP) then
						fakethok.flags2 = $ | MF2_OBJECTFLIP
						fakethok.eflags = $ | MFE_VERTICALFLIP
					end
				end
			end
		end
	end
end)
 
addHook("ThinkFrame", function(p) --idk why do I need to spawn different ones when jumping/rolling but that's it
	for p in players.iterate do
		if p.spectator then return false end
		if not p.realmo then return false end
		if p.glowaura == true then
			if (p.mo.skin == "sonic") or (p.mo.skin == "metalsonic") or (p.mo.skin == "shadow") then
				if (p.mo.state == S_PLAY_ROLL) then
					local fakethok = P_SpawnMobjFromMobj(p.mo, 0*FRACUNIT, 0*FRACUNIT, -3*FRACUNIT, MT_FAKETHOK)
						if (p.mo.skin == "shadow") and (p.mo.color == SKINCOLOR_BLACK) then
							fakethok.color = SKINCOLOR_YELLOW
						elseif (p.mo.skin == "metalsonic") and (p.mo.color == SKINCOLOR_COBALT) then
							fakethok.color = SKINCOLOR_PURPLE
						elseif (p.mo.skin == "sonic") and (p.powers[pw_super] and mrce_hyperunlocked == true) then
							fakethok.color = SKINCOLOR_WHITE
						else
							fakethok.color = p.mo.color
						end
					fakethok.blendmode = AST_ADD
					if (p.mo.flags2 & MF2_OBJECTFLIP) and (p.mo.eflags & MFE_VERTICALFLIP) then
						fakethok.flags2 = $ | MF2_OBJECTFLIP
						fakethok.eflags = $ | MFE_VERTICALFLIP
					end
				end
			end
		end
	end
end)
 
addHook("ThinkFrame", function(p) --for spinning
	for p in players.iterate do
		if p.glowaura == true then
			if (p.mo.skin == "sonic") or (p.mo.skin == "metalsonic") or (p.mo.skin == "shadow") then
				if (p.mo.state == S_PLAY_SPINDASH) then
					local fakespin = P_SpawnMobjFromMobj(p.mo, 0*FRACUNIT, 0*FRACUNIT, -3*FRACUNIT, MT_FAKESPIN)
						if (p.mo.skin == "shadow") and (p.mo.color == SKINCOLOR_BLACK) then
							fakespin.color = SKINCOLOR_YELLOW
						elseif (p.mo.skin == "metalsonic") and (p.mo.color == SKINCOLOR_COBALT) then
							fakespin.color = SKINCOLOR_PURPLE
						elseif (p.mo.skin == "sonic") and (p.powers[pw_super] and mrce_hyperunlocked == true) then
							fakespin.color = SKINCOLOR_WHITE
						else
							fakespin.color = p.mo.color
						end
					fakespin.blendmode = AST_ADD
					if (p.mo.flags2 & MF2_OBJECTFLIP) and (p.mo.eflags & MFE_VERTICALFLIP) then
						fakespin.flags2 = $ | MF2_OBJECTFLIP
						fakespin.eflags = $ | MFE_VERTICALFLIP
					end
					fakespin.angle = p.drawangle
				end
			end
		end
	end
end)

addHook("MobjThinker", function(mobj)
	if mobj.interpmode != nil then -- interpmode is never nil on uncapped
        mobj.interpmode = 2 -- set interpmode 1 for smooth no interp
	end
end, MT_FAKESPIN)

addHook("MobjThinker", function(mobj)
	for p in players.iterate do
		if p.glowaura != true then return false end
		mobj.blendmode = AST_ADD
	end
end, MT_THOK)

addHook("MobjThinker", function(mobj)
	if mobj.interpmode != nil then -- interpmode is never nil on uncapped
        mobj.interpmode = 2 -- set interpmode 1 for smooth no interp
	end
end, MT_FAKETHOK)