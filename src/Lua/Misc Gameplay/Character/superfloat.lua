addHook("JumpSpinSpecial", function(player)
	if player.mo and player.mo.skin == "sonic"
	and gamemap != 136
	and player.speed > 5*player.mo.scale
	and player.powers[pw_super]
	and P_MobjFlip(player.mo)*player.mo.momz <= 0 then
		if player.speed >= FixedMul(player.runspeed, player.mo.scale) then
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, 0)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
end)