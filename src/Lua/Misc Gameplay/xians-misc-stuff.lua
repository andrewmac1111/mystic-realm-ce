//fall animations. bc fuck airwalk, all my homies hate airwalk

addHook("PlayerThink", function(p)
	if p.spectator then return false end
	if not p.realmo then return false end
	if p.mo.state == S_PLAY_STND or p.mo.state == S_PLAY_WAIT or p.mo.state == S_PLAY_WALK or p.mo.state == S_PLAY_RUN then
		if p.mo.skin != "nasya" and p.mo.skin != "ssnsonic" and p.mo.skin != "srbii" then
			if not P_IsObjectOnGround(p.mo)
			and p.powers[pw_carry] != CR_ROLLOUT
			and p.powers[pw_carry] != CR_MINECART
			and p.powers[pw_carry] != CR_MACESWING
			and p.powers[pw_carry] != CR_ZOOMTUBE
			and p.powers[pw_carry] != CR_ROPEHANG
			and p.powers[pw_carry] != CR_PLAYER
			and p.powers[pw_carry] != CR_NIGHTSMODE then
				if p.mo.momz >= 8 then
					p.mo.state = S_PLAY_SPRING
				else if p.mo.momz <= -8 then
					p.mo.state = S_PLAY_FALL
					end
				end
			end
		end
	end
end)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end --don't want players interacting with the emerald
end, MT_EMERALD1)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end
end, MT_EMERALD2)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end
end, MT_EMERALD3)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end
end, MT_EMERALD4)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end
end, MT_EMERALD5)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end
end, MT_EMERALD6)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 then return true end
end, MT_EMERALD7)

//despawns ring maces in ultimate mode. also takes away any cheated rings

addHook("PlayerThink", function(p)
	if p.rings > 0 and ultimatemode and gamemap != 136 then
		p.rings = 0
	end
end)

--thx inferno

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid then return end

    if not ultimatemode then return end
    
    if not mobj.hprev then return end

    if mobj.hprev.type != MT_RING and mobj.hprev.type != MT_CUSTOMMACEPOINT then return end

    P_RemoveMobj(mobj)
end, MT_RING)

/*addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	
	if mobj.eflags & ~MFE_TOUCHWATER and mobj.eflags & ~MFE_UNDERWATER
		P_RemoveMobj(mobj)
	end
end, MT_SHOCKWAVE)*/

//every character go super in dwz except those that break

addHook("PlayerSpawn", function(p)
    if p.mo and p.mo.valid and gamemap == 136 and not (p.mo.skin == "sms" or p.mo.skin == "msonic") then
        if not (p.charflags & SF_SUPER) then
            p.charflags = $1|SF_SUPER
        end
    end
end)

//if p.speed == go fast, allow the player to run on water. aka, momentum based water running

addHook("PlayerThink", function(p)
	if p.spectator then return false end
	if not p.realmo then return false end
	if (p.speed >= 55*FRACUNIT) and not (skins[p.mo.skin].flags & SF_RUNONWATER) then
		p.charflags = $1|SF_RUNONWATER
	else
		if not (skins[p.mo.skin].flags & SF_RUNONWATER) and p.speed <= 55*FRACUNIT then
			p.charflags = $1 & ~(SF_RUNONWATER)
		end
	end
end)

//credit to katsy for this tiny function pulled from reveries. it's so simple I could've written it myself, but eh.
--Makes elemental shield not render its fire when in water. Because fire can't burn in water. Obviously.

addHook("MobjThinker", function(shield)
	if (shield.target) and shield.target.valid then
		if (shield.target.type == MT_ELEMENTAL_ORB) then
			if ((shield.target.target.eflags & MFE_UNDERWATER or shield.target.target.eflags & MFE_TOUCHWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA)) then
				shield.flags2 = $|MF2_DONTDRAW
			elseif (shield.flags2 & MF2_DONTDRAW) then
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
		end
	end
end, MT_OVERLAY)

//cheat codes. they're mostly here for debug purposes, but they're fun to mess with, so I'll likely be keeping them here.
//currently only 2 cheats are present; hyper cheat, which gives all 7 emeralds and unlocks hyper form, 
//and fly cheat, which allows super sonic to fly outside of dwz
//update, glow aura adds blendmode aura to sonic's rebound dash , jump ball, spindash, and roll. Was maybe thinking as a 100% complete reward, until then, it's an easter egg

COM_AddCommand("mrsecret", function(player, arg)
	if arg and arg == "4126" and not netgame then
		player.hypercheat = true
	elseif arg == "20071101" then// and not netgame
		player.flycheat = true
	elseif arg == "0" or arg == "off" then
		player.flycheat = false
		player.hypercheat = false
	elseif arg == "1207" or arg == "glow" then
		if player.glowaura == true then
			player.glowaura = false
		elseif player.glowaura == false or player.glowaura == nil then
			player.glowaura = true
		end
	else
		error("invalid input")
//	else
//		print("You can't use this in a netgame")
	end
end, 0)

addHook("PlayerThink", function(p)
	if p.hypercheat == true then
		mrce_hyperunlocked = true
		if not All7Emeralds(emeralds) then
			emeralds = 127
		end
		if modifiedgame == false and not netgame then
			COM_BufInsertText(p, "devmode 1")
			COM_BufInsertText(p, "devmode 0")
		end
	end
	if p.flycheat == true and modifiedgame == false and not netgame then
		COM_BufInsertText(p, "devmode 1")
		COM_BufInsertText(p, "devmode 0")
	end
end)