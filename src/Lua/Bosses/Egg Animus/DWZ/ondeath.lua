//this will force exit level to the credits when the final boss is defeated. If you intend to use this boss outside of mrce, do not include this script, and freeslot
//him in the soc with the rest of the boss. Or you could adapt this script for your own use, I don't care.
//The purpose of this script is to skip the score tally screen and move straight to the credits when the boss is defeated.

freeslot("MT_XBOSS") // Freeslotting so I can reference it without breaking anything, since the rest of the enemy is defined in SOC.

local disruption = 105
addHook("BossThinker", function(mo)
if not (mo.valid) then return end
	if mo.type == MT_XBOSS and mo.health <= 0 and disruption > 1 then
		disruption = $ - 1
		if disruption <= 1 then
			G_SetCustomExitVars(98,1)
			disruption = 5000
			G_ExitLevel()
		end
	end
end)