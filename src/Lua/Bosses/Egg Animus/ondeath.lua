//this will force exit level to MAPB0 when the Egg Animus is defeated. If you intend to use the egg animus outside of mrce, do not include this script, and freeslot
//the second phase in the soc with the rest of the boss. Or you could adapt this script for your own use, I don't care.
//The purpose of this script is to skip the score tally screen and move straight to Dimension Warp when the boss is defeated.

freeslot("MT_EGGANIMUS_EX") // Freeslotting so I can reference it without breaking anything, since the rest of the enemy is defined in SOC.

local disruption = 7 * TICRATE
addHook("BossThinker", function(mo)
if not (mo.valid) then return end
	if mo.type == MT_EGGANIMUS_EX and mo.health <= 0 then
		disruption = $ - 1
		if disruption <= 0 then
			disruption = 10 * TICRATE
			G_SetCustomExitVars(136,1)
			G_ExitLevel()
		end
	end
end)