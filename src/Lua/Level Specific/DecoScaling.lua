addHook("MapThingSpawn", function(mo, mt)
    if (mo == nil) or (mt == nil) then return end
    local info = mobjinfo[mo.type]
    
    if (mo.flags & MF_SCENERY) and (mapheaderinfo[gamemap].fakeudmf != nil) then
        local s = mapheaderinfo[gamemap].fakeudmf * mt.extrainfo
        if mt.extrainfo == 0 then
            s = 100
        end
        mo.scale = ($ * s) / 100
        mo.destscale = mo.scale
    end
    if (info.flags & MF_ENEMY) and (mapheaderinfo[gamemap].enemysize != nil) then
        mo.scale = ($ * mapheaderinfo[gamemap].enemysize) / 100
        mo.destscale = mo.scale
    end
    if (info.flags & MF_MONITOR) and (mapheaderinfo[gamemap].monitorsize != nil) then
        mo.scale = ($ * mapheaderinfo[gamemap].monitorsize) / 100
        mo.destscale = mo.scale
    end
    if (mo.type == MT_RING) and (mapheaderinfo[gamemap].ringsize != nil) then
        mo.scale = ($ * mapheaderinfo[gamemap].ringsize) / 100
        mo.destscale = mo.scale
    end
    if (mo.type == MT_TOKEN) and (mapheaderinfo[gamemap].tokensize != nil) then
        mo.scale = ($ * mapheaderinfo[gamemap].tokensize) / 100
        mo.destscale = mo.scale
    end
    if (mo.type == MT_STARPOST) and (mapheaderinfo[gamemap].starpostsize != nil) then
        mo.scale = ($ *  mapheaderinfo[gamemap].starpostsize / 100)
        mo.destscale = mo.scale
    end
end)