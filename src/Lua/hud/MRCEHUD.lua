/*
MRCE Lua HUD

(C) 2020-2021 by RazzDazzle

See "MUST READ INFO" under "Razz's Misc Scripts"
for relevant info
*/

local xpos = -52
local slidein = "no"
local hticker = 0

local function DrawMRCEHUD(v, p, cam, ticker, endticker)
	if p.spectator return false end
	if not p.realmo return false end
	if hticker == 58
		if xpos <= 16
			xpos = $ + 6
		end
	else
		hticker = $ + 2
	end

	if (p.realmo) and not (p.mo.skin == "modernsonic") and not (p.mo.skin == "samus") and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap)) and gamemap != 99 then
		v.draw(xpos, 10, v.cachePatch("MRHSCORE"), V_SNAPTOLEFT|V_SNAPTOBOTTOM|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		v.draw(xpos, 26, v.cachePatch("MRHTIME"), V_SNAPTOLEFT|V_SNAPTOBOTTOM|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		if not ultimatemode
		or mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			v.draw(xpos, 42, v.cachePatch("MRHRINGS"), V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
			DrawCustomString(v, 75*FRACUNIT, 44*FRACUNIT, FRACUNIT, tostring(p.rings), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		end
		DrawCustomString(v, 75*FRACUNIT, 9*FRACUNIT, FRACUNIT, tostring(p.score), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		if G_TicsToMinutes(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawCustomString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawCustomString(v, 83*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		elseif G_TicsToMinutes(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawCustomString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			DrawCustomString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, tostring(p.warpminutes), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		end
		if G_TicsToSeconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawCustomString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawCustomString(v, 107*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif G_TicsToSeconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawCustomString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			DrawCustomString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, tostring(p.warpseconds), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		end
		if G_TicsToCentiseconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawCustomString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawCustomString(v, 131*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif G_TicsToCentiseconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawCustomString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			DrawCustomString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, tostring(p.warpcentiseconds), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		end
		v.draw(91, 27, v.cachePatch("MRCEFNTS"), V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		v.draw(115, 27, v.cachePatch("MRCEFNTP"), V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		if (p.rings == 0 and (leveltime/5%2) and not ultimatemode) or (mapheaderinfo[gamemap].lvlttl == "Dimension Warp" and p.rings <= 10 and (leveltime/5%2))
			v.draw(xpos, 42, v.cachePatch("MRHRRING"), V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		end
		hud.disable("rings")
		hud.disable("time")
		hud.disable("score")
	end
end

addHook("MapLoad", function()
	xpos = -52
end)

hud.add(DrawMRCEHUD, "game")

addHook("PlayerThink", function(p)
	if (mapheaderinfo[gamemap].lvlttl != "Dimension Warp") return end
	if not (leveltime%35)
	or (leveltime%TICRATE == TICRATE/5) or (leveltime%TICRATE == ((TICRATE/5) * 3)) or (leveltime%TICRATE == ((TICRATE/5) * 2)) or (leveltime%TICRATE == ((TICRATE/5) * 4))
		p.warpminutes =  P_RandomRange(10, 99)
		p.warpseconds =  P_RandomRange(10, 99)
		p.warpcentiseconds =  P_RandomRange(10, 99)
	end
end)
		