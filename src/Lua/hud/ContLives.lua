--coded by Sylve, SimplerHud code by SteelT referenced to figure out how to draw the life icons and such in the right places, mainly how to use v.getSprite2Patch (thank you because I was really confused on how to do hud code lol)

freeslot(
"SKINCOLOR_COMPLETEBLACKDROPSHADOW" --name is very long and very specific to avoid conflicts
)

skincolors[SKINCOLOR_COMPLETEBLACKDROPSHADOW] = {
	name = "CompleteBlack",
	ramp = {31,31,31,31,31,31,31,31,31,31,31,31,31,31,31,31},
	invcolor = SKINCOLOR_WHITE,
	invshade = 1,
	chatcolor = V_GRAYMAP,
	accessible = false
}


local function DrawLivesIcons(v, p, cam)
if gamemap == 1001 then return end -- Adding this for the sprite model viewer I used while debugging.
if p.spectator then return false end
if not p.realmo then return false end
	if ultimatemode and gamemap == 136 and (hud.enabled("lives")) then
		hud.disable("lives")
	end
	
	if (p.realmo) and not (p.hypermysticsonic) and not (p.mo.skin == "modernsonic") and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap)) and not modeattacking and gamemap != 99 and not ultimatemode then
		local contsprite = v.getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, C) or v.getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, A) or v.getSprite2Patch(p.realmo.skin, SPR2_STND, false, A, 2) --last resort is stnd, please do not do this it looks BAD
		v.draw(hudinfo[HUD_LIVES].x, hudinfo[HUD_LIVES].y-16,  v.cachePatch("MRLIVEBK"), V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, v.getColormap(p.realmo.skin, p.realmo.color))
		
		if not (p.spectator) then
			v.drawScaled((hudinfo[HUD_LIVES].x+18)*FRACUNIT, (hudinfo[HUD_LIVES].y+10)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTOBOTTOM|V_PERPLAYER|V_SNAPTOLEFT|((p.spectator) and V_HUDTRANSHALF or V_HUDTRANS), v.getColormap(TC_BLINK, SKINCOLOR_COMPLETEBLACKDROPSHADOW)) --dropshadow goes first
			v.drawScaled((hudinfo[HUD_LIVES].x+17)*FRACUNIT, (hudinfo[HUD_LIVES].y+8)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTOBOTTOM|V_PERPLAYER|V_SNAPTOLEFT|V_HUDTRANS, v.getColormap(p.realmo.skin, p.realmo.color)) --then the actual player sprite
		else
			v.drawScaled((hudinfo[HUD_LIVES].x+17)*FRACUNIT, (hudinfo[HUD_LIVES].y+3)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTOBOTTOM|V_PERPLAYER|V_SNAPTOLEFT|((p.spectator) and V_HUDTRANSHALF), v.getColormap(TC_RAINBOW, p.realmo.color)) --there is no dropshadow when you're spectating; you're a ghost
		end
		
			
		
		if (hud.enabled("lives")) then 
			hud.disable("lives")
		end
		
		if (customhud) then --not you either custom hud
			customhud.disable("lives")
		end
		
		if (G_GametypeUsesLives()) then
			if not (p.lives == INFLIVES) and not (cv_cooplives == "Infinite") then --there is nothing of the sort in gamemodes without lives
				DrawCustomString(v, (hudinfo[HUD_LIVES].x+68)*FRACUNIT, (hudinfo[HUD_LIVES].y+5)*FRACUNIT, FRACUNIT, tostring(p.lives), "MRCEFNT", V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			end
		end
		
	end
end

hud.add(DrawLivesIcons, "game")

