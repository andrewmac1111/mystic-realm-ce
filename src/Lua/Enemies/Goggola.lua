--[[
Goggles Crawla - "Goggola"

SOC by Zippy_Zolton

Lua conversion done by Ryuko
--]]

freeslot(
	"SPR_GOGA",
	"MT_GOGGLESCRAWLA_SLOW",
	"MT_GOGGLESCRAWLA_FAST",
	"S_JOSS_STND",
	"S_JOSS_WALK1",
	"S_JOSS_WALK2",
	"S_JOSS_WALK3",
	"S_JOSS_WALK4",
	"S_JOSS_WALK5",
	"S_JOSS_WALK6",
	"S_JOSS_CHARGE1",
	"S_JOSS_CHARGE2",
	"S_JOSS_RUN1",
	"S_JOSS_RUN2",
	"S_JOSS_RUN3",
	"S_JOSS_RUN4",
	"S_JOSS_RUN5",
	"S_JOSS_RUN6",
	"S_JOSS_STOP"
)

-- "Goggles Crawla, may have colored variants later"

mobjinfo[MT_GOGGLESCRAWLA_SLOW] = {
	--$Name Goggles Crawla (Slow)
	--$Sprite GOGAA1
	--$Category Mystic Realm Enemies
	doomednum = 2302,
	SpawnState = S_JOSS_STND,
	SpawnHealth = 1,
	SeeState = S_JOSS_WALK1,
	ReactionTime = 35,
	AttackSound = sfx_s3kb6,
	PainState = S_NULL,
	PainChance = 5*TICRATE,
	MeleeState = S_JOSS_STOP,
	MissileState = S_JOSS_CHARGE1,
	DeathState = S_XPLD_FLICKY,
	XDeathState = S_JOSS_STOP,
	DeathSound = sfx_pop,
	Speed = 1,
	Radius = 24*FRACUNIT,
	Height = 32*FRACUNIT,
	Mass = 100,
	Damage = 0,
	ActiveSound = sfx_s3k5a,
	RaiseState = S_NULL,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE
}

mobjinfo[MT_GOGGLESCRAWLA_FAST] = {
	--$Name Goggles Crawla (Fast)
	--$Sprite GOGAA1
	--$Category Mystic Realm Enemies
	doomednum = 2303,
	SpawnState = S_JOSS_STND,
	SpawnHealth = 1,
	SeeState = S_JOSS_WALK1,
	SeeSound = sfx_none,
	ReactionTime = 35,
	AttackSound = sfx_s3kb6,
	PainState = S_NULL,
	PainChance = 5*TICRATE,
	PainSound = sfx_none,
	MeleeState = S_JOSS_STOP,
	MissileState = S_JOSS_CHARGE1,
	DeathState = S_XPLD_FLICKY,
	XDeathState = S_JOSS_STOP,
	DeathSound = sfx_pop,
	Speed = 1,
	Radius = 24*FRACUNIT,
	Height = 32*FRACUNIT,
	Mass = 100,
	Damage = 0,
	ActiveSound = sfx_s3k5a,
	Flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE,
	RaiseState = S_NULL
}

states[S_JOSS_STND] = {
	sprite = GOGA,
	frame = A,
	tics = 5,
	nextstate = S_JOSS_STND,
	Action = A_Look,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_WALK1] = {
	sprite = GOGA,
	frame = A,
	tics = 3,
	nextstate = S_JOSS_WALK2,
	Action = A_SharpChase,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_WALK2] = {
	sprite = GOGA,
	frame = B,
	tics = 3,
	nextstate = S_JOSS_WALK3,
	Action = A_SharpChase,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_WALK3] = {
	sprite = GOGA,
	frame = C,
	tics = 3,
	nextstate = S_JOSS_WALK4,
	Action = A_SharpChase,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_WALK4] = {
	sprite = GOGA,
	frame = D,
	tics = 3,
	nextstate = S_JOSS_WALK1,
	Action = A_SharpChase,
	Var1 = 0,
	Var2 = 0
}

-- "Charging state because A_SharpSpin sucks ass"

states[S_JOSS_CHARGE1] = {
	sprite = GOGA,
	frame = A,
	tics = 1,
	nextstate = S_JOSS_CHARGE1,
	Action = A_FaceStabRev,
	Var1 = 35,
	Var2 = S_JOSS_CHARGE2
}

states[S_JOSS_CHARGE2] = {
	sprite = GOGA,
	frame = A,
	tics = 0,
	nextstate = S_JOSS_RUN1,
	Action = A_PlayAttackSound,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_RUN1] = {
	sprite = GOGA,
	frame = A,
	tics = 1,
	nextstate = S_JOSS_RUN2,
	Action = A_SharpSpin,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_RUN2] = {
	sprite = GOGA,
	frame = B,
	tics = 1,
	nextstate = S_JOSS_RUN3,
	Action = A_SharpSpin,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_RUN3] = {
	sprite = GOGA,
	frame = C,
	tics = 1,
	nextstate = S_JOSS_RUN4,
	Action = A_SharpSpin,
	Var1 = 0,
	Var2 = 0
}

states[S_JOSS_RUN4] = {
	sprite = GOGA,
	frame = D,
	tics = 1,
	nextstate = S_JOSS_RUN1,
	Action = A_SharpSpin,
	Var1 = 0,
	Var2 = 0
}
-- "Stop state for the same fucking reason"

states[S_JOSS_STOP] = {
	sprite = GOGA,
	frame = A,
	tics = 1,
	nextstate = S_JOSS_STND,
	Action = none,
	Var1 = 0,
	Var2 = 0
}
