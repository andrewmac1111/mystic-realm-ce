/*
         MRCE Init.lua Script

                by Ashi

    Files are loaded from top to bottom.
    Any typos in paths or names will cause it to
    throw an error and stop loading files.

    errors from scripts that were successfuly found 
    and loaded will not cause this behavior however.
*/

-- Performs a version check. Will only show a warning if subversion check fails.
if VERSION != 202 then -- Just in case 2.3 breaks 2.2 mods or something
    error("\
                               No Way? No Way!\
                 MRCE Requires SRB2 version 2.2.10 or higher.\
              You have version 2.3 or a version older than 2.2.0!\
           Grab the latest version of 2.2 from srb2.org to play MRCE!", 0) return
elseif SUBVERSION < 10 then
    -- No Way! code for older SRB2 variants. Enable on 2.2.10's full release. Bug reports
    -- from unsupported versions are super annoying and I'd like to avoid them.
    dofile("GameCode/NoWayNoWay.lua")
    error("No Way? No Way!\nSRB2 Version 2.2.10 or higher is required but was not found.\nLoad halted.", 0) return
end


for _, filename in ipairs{
    -- ALWAYS Load GlobalFreeslot.lua first!!
    "GlobalFreeslot.lua",

    -- custom save and unlock system
    //"GameCode/customsavesystem/savefunctions.lua",
    //"GameCode/customsavesystem/unlockables.lua",
    //"GameCode/customsavesystem/debug.lua",

    -- Sprite model framework
    "GameCode/sprmdl/sprmdl_framework.lua",
    "GameCode/sprmdl/mdlviewer_main.lua",
    "GameCode/sprmdl/mdlviewer_gui.lua",

    -- Loading HUD and GUI scripts
    "HUD/MRCEHUD.lua",
    "HUD/ContLives.lua",
    "HUD/NewEmblemHUD.lua",
    "HUD/Intermission.lua",

    -- Load menu code
    "Menu/Title/title_animation.lua",
    -- TODO: Custom main menu (when 2.2.10 releases)
    -- "Menu/mainmenu.lua", -- Work in Progress custom main menu
    "Menu/episode_select.lua",
    -- Extras
    /*WIP*/"Menu/Extras/making_of_mystic_realm.lua",
    /*WIP*/"Menu/Extras/credits.lua",

    -- Load all the gameplay changes
    "Misc Gameplay/EXAI.lua",
    "Misc Gameplay/jumpleniency.lua",
    "Misc Gameplay/MMTM.lua",
    "Misc Gameplay/xians-misc-stuff.lua",
    "Misc Gameplay/hyperforms/HYPER.lua",
    "Misc Gameplay/hyperforms/STF.lua",
    "Misc Gameplay/hyperforms/misc.lua",
    "Misc Gameplay/Character/ReboundDash.lua",
    "Misc Gameplay/Character/superfloat.lua",
    "Misc Gameplay/Character/FangBombThrow.lua",

    -- Level Code
    "Level Specific/Emerald Stages/Mystic_Shrines.lua",
    "Level Specific/Emerald Stages/emeralds.lua",
    "Level Specific/UtilityMap/dontdraw.lua",
    "Level Specific/DecoScaling.lua",
    "Level Specific/Aerial Garden/ExitStage.lua",

    -- Dimension Warp
    "Level Specific/dimensionwarp/kill.lua",
    "Level Specific/dimensionwarp/decay.lua",
    "Level Specific/dimensionwarp/reveriefly.lua",
    "Level Specific/dimensionwarp/HMScheat.lua",

    -- Primordial Abyss
    "Level Specific/primordialabyss/NOTAILS.lua",

    -- Objects
    "Objects/CAKE.lua",
    "Objects/Cryocrawla.lua",
    "Objects/forcerollsprings.lua",
    "Objects/KHZDeco.lua",
    "Objects/NewEmblems.lua",
    "Objects/slowgoop.lua",
    "Objects/rock/COLL.lua",
    "Objects/rock/ROCK.lua",
    //"Objects/Slime.lua", -- Code is wonky

    -- Enemies
    "Enemies/DerelictCrawla.lua",
    "Enemies/Goggola.lua",
    "Enemies/Goopla.lua",
    //"Enemies/Octo.lua", -- Code is wonky

    -- Bosses (Zone Order)
    "Bosses/EggDecker.lua",

    "Bosses/Egg Baller/Freeslot.lua",
    "Bosses/Egg Baller/Helpers.lua",
    "Bosses/Egg Baller/I_Boss.lua",

    "Bosses/EggFreezer.lua",
    "Bosses/EggBomber.lua",

    -- Egg Animus (MRZ)
    "Bosses/Egg Animus/ondeath.lua",

    -- Egg Animus (DWZ)
    "Bosses/Egg Animus/DWZ/ondeath.lua",
    "Bosses/Egg Animus/DWZ/Freeslot.lua",
    "Bosses/Egg Animus/DWZ/BossPrototype.lua"
} do
    dofile(filename)
end