# The Mystic Realm Community Edition repository

Contains:
- Latest version of the mod (most of the time)
- Issues related to the mod
- Progress tracking for the mod
- All the files required to help out with the mod and more

## Contributing to MRCE
You can contribute your work to MRCE via the [Discord Server](https://discord.gg/WcB2vaqbgf)

## Building MRCE

### Windows
Run create_pk3.bat and load the addon through SRB2

NOTE: It will also search through /src to fix up any files with the .lmp extension, which may take some time if there's a lot of them.

### Linux/macOS
NOTE: You will need 7z installed on your system for this to work\
	  On macOS this can be found inside MacPorts or Homebrew

Open a terminal and navigate to the source folder\
Run create_pk3.sh and load the addon through SRB2

NOTE: Unlike the Windows create_pk3. This one WON'T remove .lmp from files.\
      It also doesn't make clean builds so you might have to delete the pk3 if the build isn't coming out right.



